﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Hardware_Inventory.ConsoleClient
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;
    using Newtonsoft.Json;

    public class Colleague
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Mail { get; set; }
        public string Workplace { get; set; }
        public string Post { get; set; }

        public override string ToString()
        {
            return $"Id: {ID} \n Full name: {Name} \n E-mail address: {Mail} \n Post: {Post} \n Which HQ: {Workplace}";
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Waiting...");
            Console.ReadLine();

            string url = "http://localhost:51284/api/ColleaguesApi/";
            using (HttpClient client = new HttpClient())
            {
                string json = client.GetStringAsync(url + "all").Result;
                var list = JsonConvert.DeserializeObject<List<Colleague>>(json);
                foreach (var item in list)
                {
                    Console.WriteLine(item);
                }
                Console.ReadKey();
                Console.WriteLine("NEW CHALLENGER APPROACHES");
                Console.ReadKey();
                Dictionary<string, string> postData;
                string response;
                postData = new Dictionary<string, string>();
                postData.Add(nameof(Colleague.Name), "Donald Trump");
                postData.Add(nameof(Colleague.Mail), "twitter@realtrump");
                postData.Add(nameof(Colleague.Post), "president");
                postData.Add(nameof(Colleague.Workplace), "white house yo.");

                response = client.PostAsync(url + "add", new FormUrlEncodedContent
                    (postData)).Result.Content.ReadAsStringAsync().Result;

                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("ADD:"+ response);
                Console.WriteLine("ALL" + json);
                Console.ReadLine();

                int ColleagueID = JsonConvert.DeserializeObject<List<Colleague>>(json).Single(x => x.Name == "Donald Trump").ID;
                postData = new Dictionary<string, string>();
                postData.Add(nameof(Colleague.ID), ColleagueID.ToString());
                postData.Add(nameof(Colleague.Name), "Donald Trump");
                postData.Add(nameof(Colleague.Mail), "twitter@realtrump");
                postData.Add(nameof(Colleague.Post), "impeached");
                postData.Add(nameof(Colleague.Workplace), "Ivanka's garage.");

                response = client.PostAsync(url + "modify", new FormUrlEncodedContent
                    (postData)).Result.Content.ReadAsStringAsync().Result;

                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("MODIFY:" + response);
                Console.WriteLine("ALL" + json);
                Console.ReadLine();

                Console.WriteLine("Deleting TRUMP");
                response = client.GetStringAsync(url + "del/" + ColleagueID).Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("DELL:" + response);
                Console.WriteLine("ALL" + json);
                Console.ReadLine();
            }
        }
    }
}
