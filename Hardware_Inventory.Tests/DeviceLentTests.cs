﻿// <copyright file="DeviceLentTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Hardware_Inventory.Tests
{
    using System.Collections.Generic;
    using System.Linq;
    using Hardware_Inventory.BL.Services;
    using Hardware_Inventory.DA;
    using Hardware_Inventory.Repository.Interfaces;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Unit tests for DeviceLentsService.
    /// </summary>
    [TestFixture]
    public class DeviceLentTests
    {
        private List<DeviceLent> deviceLents;
        private Mock<IDeviceLentRepository> deviceLentsMocked;

        /// <summary>
        /// Sets up the enviroment for testing purposes.
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            this.deviceLents = new List<DeviceLent>()
            {
                new DeviceLent()
                {
                    Users_ID = 22,
                    Device_ID = 33,
                },
                new DeviceLent()
                {
                    Users_ID = 1,
                    Device_ID = 2,
                },
                new DeviceLent()
                {
                    Users_ID = 42,
                    Device_ID = 24,
                },
            };

            this.deviceLentsMocked = new Mock<IDeviceLentRepository>();
            this.deviceLentsMocked.Setup(x => x.GetAll()).Returns(this.deviceLents.AsQueryable);
        }

        /// <summary>
        /// Tests if we really get back all the DeviceLents.
        /// </summary>
        [Test]
        public void Get_All_DeviceLents_Test()
        {
            // Arrange
            DeviceLentService test = new DeviceLentService(this.deviceLentsMocked.Object);

            // Act
            var act = test.GetAll();

            // Assert
            Mock.Verify(this.deviceLentsMocked);
            CollectionAssert.AreEquivalent(this.deviceLents, act);
        }

        /// <summary>
        /// Tests if we can add a DeviceLent.
        /// </summary>
        [Test]
        public void Add_New_DeviceLent_Test()
        {
            // Arrange
            var deviceLentToBeAdded = new DeviceLent()
            {
                Users_ID = 55,
                Device_ID = 55,
            };
            this.deviceLentsMocked.Setup(x => x.AddNewDeviceLent(deviceLentToBeAdded)).Callback(() => this.deviceLents.Add(deviceLentToBeAdded));
            DeviceLentService test = new DeviceLentService(this.deviceLentsMocked.Object);

            // Act
            test.AddNewDeviceLent(deviceLentToBeAdded);
            var newcount = test.GetAll().Count;

            // Assert
            this.deviceLentsMocked.Verify(x => x.AddNewDeviceLent(deviceLentToBeAdded), Times.Once);
            CollectionAssert.Contains(this.deviceLents, deviceLentToBeAdded);
        }

        /// <summary>
        /// Tests if we are capable of getting the correct DeviceLent back upon searching via Device ID.
        /// </summary>
        [Test]
        public void Get_DeviceLent_By_Device_ID_Test()
        {
            // Arrange
            this.deviceLentsMocked.Setup(x => x.GetDeviceLentByDeviceId(33)).Returns(this.deviceLents.First());
            var deviceLentToFind = new DeviceLent()
            {
                Users_ID = 22,
                Device_ID = 33,
            };
            DeviceLentService test = new DeviceLentService(this.deviceLentsMocked.Object);

            // Act
            DeviceLent found = test.GetDeviceLentByDeviceID(deviceLentToFind.Device_ID);

            // Assert
            Mock.Verify(this.deviceLentsMocked);
            Assert.That(found.Device_ID, Is.EqualTo(deviceLentToFind.Device_ID));
        }

        /// <summary>
        /// Tests if we are capable of getting the correct DeviceLent back upon searching via Device ID.
        /// </summary>
        [Test]
        public void Get_DeviceLent_By_User_ID_Test()
        {
            // Arrange
            this.deviceLentsMocked.Setup(x => x.GetDeviceLentByUserId(22)).Returns(this.deviceLents.First());
            var deviceLentToFind = new DeviceLent()
            {
                Users_ID = 22,
                Device_ID = 33,
            };
            DeviceLentService test = new DeviceLentService(this.deviceLentsMocked.Object);

            // Act
            DeviceLent found = test.GetDeviceLentByUserID(deviceLentToFind.Users_ID);

            // Assert
            Mock.Verify(this.deviceLentsMocked);
            Assert.That(found.Users_ID, Is.EqualTo(deviceLentToFind.Users_ID));
        }

        /// <summary>
        /// Test whether we can we delete a DeviceLent.
        /// </summary>
        [Test]
        public void Delete_DeviceLent_Test()
        {
            // Arrange
            var deviceLentToBeDeleted = new DeviceLent()
            {
                Users_ID = 22,
                Device_ID = 33,
            };
            this.deviceLentsMocked.Setup(x => x.Delete(deviceLentToBeDeleted.Users_ID, deviceLentToBeDeleted.Device_ID)).Callback(() => this.deviceLents.Remove(deviceLentToBeDeleted));
            DeviceLentService test = new DeviceLentService(this.deviceLentsMocked.Object);

            // Act
            test.Delete(deviceLentToBeDeleted.Users_ID, deviceLentToBeDeleted.Device_ID);

            // Assert
            Mock.Verify(this.deviceLentsMocked);
            CollectionAssert.DoesNotContain(this.deviceLents, deviceLentToBeDeleted);
        }
    }
}
