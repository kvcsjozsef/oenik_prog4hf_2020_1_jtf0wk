﻿// <copyright file="ModellTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Hardware_Inventory.Tests
{
    using System.Collections.Generic;
    using System.Linq;
    using Hardware_Inventory.BL.Services;
    using Hardware_Inventory.DA;
    using Hardware_Inventory.Repository.Interfaces;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Unit tests for ModellsService.
    /// </summary>
    [TestFixture]
    public class ModellTests
    {
        private List<Modell> modells;
        private Mock<IModellRepository> modellsMocked;

        /// <summary>
        /// Sets up the enviroment for testing purposes.
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            this.modells = new List<Modell>()
            {
                new Modell()
                {
                    Modell_ID = "Almás Nokia",
                    DeviceType = "Payphone",
                },
                new Modell()
                {
                    Modell_ID = "Custom Built Pc",
                    DeviceType = "GLORIOUS GAMER ARRHAGHGHH",
                },
                new Modell()
                {
                    Modell_ID = "Lenovo X1",
                    DeviceType = "business laptop",
                },
            };

            this.modellsMocked = new Mock<IModellRepository>();
            this.modellsMocked.Setup(x => x.GetAll()).Returns(this.modells.AsQueryable);
        }

        /// <summary>
        /// Tests if we really get back all the Modells.
        /// </summary>
        [Test]
        public void Get_All_Modells_Test()
        {
            // Arrange
            ModellService test = new ModellService(this.modellsMocked.Object);

            // Act
            var act = test.GetAll();

            // Assert
            Mock.Verify(this.modellsMocked);
            CollectionAssert.AreEquivalent(this.modells, act);
        }

        /// <summary>
        /// Tests if we can add a Modell.
        /// </summary>
        [Test]
        public void Add_New_Modell_Test()
        {
            // Arrange
            var modellToBeAdded = new Modell()
            {
                Modell_ID = "Dell Vostro",
                DeviceType = "business laptop???!!",
            };
            this.modellsMocked.Setup(x => x.AddNewModell(modellToBeAdded)).Callback(() => this.modells.Add(modellToBeAdded));
            ModellService test = new ModellService(this.modellsMocked.Object);

            // Act
            test.AddNewModell(modellToBeAdded);
            var newcount = test.GetAll().Count;

            // Assert
            this.modellsMocked.Verify(x => x.AddNewModell(modellToBeAdded), Times.Once);
            CollectionAssert.Contains(this.modells, modellToBeAdded);
        }

        /// <summary>
        /// Test whether we can we delete a Modell.
        /// </summary>
        [Test]
        public void Delete_Modell_Test()
        {
            // Arrange
            var modellToBeDeleted = new Modell()
            {
                Modell_ID = "Almás Nokia",
                DeviceType = "Payphone",
            };
            this.modellsMocked.Setup(x => x.Delete(modellToBeDeleted.Modell_ID)).Callback(() => this.modells.Remove(modellToBeDeleted));
            ModellService test = new ModellService(this.modellsMocked.Object);

            // Act
            test.Delete(modellToBeDeleted.Modell_ID);

            // Assert
            Mock.Verify(this.modellsMocked);
            CollectionAssert.DoesNotContain(this.modells, modellToBeDeleted);
        }

        /// <summary>
        /// Tests if we are capable of getting the correct Modell back upon searching via ID.
        /// </summary>
        [Test]
        public void Get_Modell_By_ID_Test()
        {
            // Arrange
            this.modellsMocked.Setup(x => x.GetModellById("Almás Nokia")).Returns(this.modells.First());
            var modellToFind = new Modell()
            {
                Modell_ID = "Almás Nokia",
                DeviceType = "Payphone",
            };
            ModellService test = new ModellService(this.modellsMocked.Object);

            // Act
            Modell found = test.GetModellByID(modellToFind.Modell_ID);

            // Assert
            Mock.Verify(this.modellsMocked);
            Assert.That(found.Modell_ID, Is.EqualTo(modellToFind.Modell_ID));
        }
    }
}
