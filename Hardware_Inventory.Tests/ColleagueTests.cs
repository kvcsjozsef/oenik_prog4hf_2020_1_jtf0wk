﻿// <copyright file="ColleagueTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Hardware_Inventory.Tests
{
    using System.Collections.Generic;
    using System.Linq;
    using Hardware_Inventory.BL.Services;
    using Hardware_Inventory.DA;
    using Hardware_Inventory.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Unit tests for ColleaguesService.
    /// </summary>
    [TestFixture]
    public class ColleagueTests
    {
        private List<colleagues> colleagues;
        private Mock<IColleaguesRepository> colleaguesMocked;

        /// <summary>
        /// Sets up the basic mock db for the testing.
        /// </summary>
        [SetUp]
        public void SetUp()
        {
        this.colleagues = new List<colleagues>()
            {
                new colleagues()
                {
                    Users_ID = 1,
                    Full_Name = "Kis Béla",
                    WorksAt = "Otthon",
                },
                new colleagues()
                {
                    Users_ID = 2,
                    Full_Name = "Trap Pista",
                    WorksAt = "Budapest",
                },
                new colleagues()
                {
                    Users_ID = 3,
                    Full_Name = "Elemér",
                    WorksAt = "Best Prog Tanár",
                },
            };

        this.colleaguesMocked = new Mock<IColleaguesRepository>();
        this.colleaguesMocked.Setup(x => x.GetAll()).Returns(this.colleagues.AsQueryable);
    }

        /// <summary>
        /// Tests if we really do get back all the colleagues.
        /// </summary>
        [Test]
        public void Get_All_Colleagues_Test()
        {
            // Arrange
            ColleagueService test = new ColleagueService(this.colleaguesMocked.Object);

            // Act
            var act = test.GetAll();

            // Assert
            Mock.Verify(this.colleaguesMocked);
            CollectionAssert.AreEquivalent(this.colleagues, act);
        }

        /// <summary>
        /// Test if we can really add a new colleague.
        /// </summary>
        [Test]
        public void Add_New_Colleague_Test()
        {
            // Arrange
            var colleagueToBeAdded = new colleagues()
            {
                Users_ID = 4,
                Full_Name = "Új Ember",
                WorksAt = "Budapest",
            };
            this.colleaguesMocked.Setup(x => x.AddNewColleague(colleagueToBeAdded)).Callback(() => this.colleagues.Add(colleagueToBeAdded));
            ColleagueService test = new ColleagueService(this.colleaguesMocked.Object);

            // Act
            test.AddNewColleague(colleagueToBeAdded);
            var newcount = test.GetAll().Count;

            // Assert
            this.colleaguesMocked.Verify(x => x.AddNewColleague(colleagueToBeAdded), Times.Once);
            CollectionAssert.Contains(this.colleagues, colleagueToBeAdded);
        }

        /// <summary>
        /// Test if we really can delete a colleague.
        /// </summary>
        [Test]
        public void Delete_Colleague_Test()
        {
            // Arrange
            var colleagueToBeDeleted = new colleagues()
            {
                Users_ID = 1,
                Full_Name = "Kis Béla",
                WorksAt = "Otthon",
            };
            this.colleaguesMocked.Setup(x => x.Delete(colleagueToBeDeleted.Users_ID)).Callback(() => this.colleagues.Remove(colleagueToBeDeleted));
            ColleagueService test = new ColleagueService(this.colleaguesMocked.Object);

            // Act
            test.Delete(colleagueToBeDeleted.Users_ID);

            // Assert
            Mock.Verify(this.colleaguesMocked);
            CollectionAssert.DoesNotContain(this.colleagues, colleagueToBeDeleted);
        }

        /// <summary>
        /// Tests if we get the correct colleague back when searching via ID.
        /// </summary>
        [Test]
        public void Get_Colleague_By_ID_Test()
        {
            // Arrange
            this.colleaguesMocked.Setup(x => x.GetColleagueById(1)).Returns(this.colleagues.First());
            var colleagueToFind = new colleagues()
            {
                Users_ID = 1,
                Full_Name = "Kis Béla",
                WorksAt = "Otthon",
            };
            ColleagueService test = new ColleagueService(this.colleaguesMocked.Object);

            // Act
            colleagues found = test.GetColleagueByID(colleagueToFind.Users_ID);

            // Assert
            Mock.Verify(this.colleaguesMocked);
            Assert.That(found.Users_ID, Is.EqualTo(colleagueToFind.Users_ID));
        }
    }
}
