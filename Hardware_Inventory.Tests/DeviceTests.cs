﻿// <copyright file="DeviceTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Hardware_Inventory.Tests
{
    using System.Collections.Generic;
    using System.Linq;
    using Hardware_Inventory.BL;
    using Hardware_Inventory.BL.Services;
    using Hardware_Inventory.DA;
    using Hardware_Inventory.Repository.Interfaces;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Unit tests for DevicesService.
    /// </summary>
    [TestFixture]
    public class DeviceTests
    {
        private List<Devices> devices;
        private Mock<IDevicesRepository> devicesMocked;

        /// <summary>
        /// Sets up the enviroment for testing purposes.
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            this.devices = new List<Devices>()
            {
                new Devices()
                {
                    Device_ID = 1,
                    Modell = "Apple Macbook",
                    State_now = "Raktáron",
                },
                new Devices()
                {
                    Device_ID = 2,
                    Modell = "Almás Nokia",
                    State_now = "Felhasználónál",
                },
                new Devices()
                {
                    Device_ID = 3,
                    Modell = "Custom Built Gamer Pc",
                    State_now = "Rossz",
                },
            };

            this.devicesMocked = new Mock<IDevicesRepository>();
            this.devicesMocked.Setup(x => x.GetAll()).Returns(this.devices.AsQueryable);
        }

        /// <summary>
        /// Tests if we really get back all the devices.
        /// </summary>
        [Test]
        public void Get_All_Devices_Test()
        {
            // Arrange
            DeviceService test = new DeviceService(this.devicesMocked.Object);

            // Act
            var act = test.GetAll();

            // Assert
            Mock.Verify(this.devicesMocked);
            CollectionAssert.AreEquivalent(this.devices, act);
        }

        /// <summary>
        /// Tests if we can add a device.
        /// </summary>
        [Test]
        public void Add_New_device_Test()
        {
            // Arrange
            var deviceToBeAdded = new Devices()
            {
                Device_ID = 4,
                Modell = "Szamszung",
                State_now = "Raktáron",
            };
            this.devicesMocked.Setup(x => x.AddNewDevice(deviceToBeAdded)).Callback(() => this.devices.Add(deviceToBeAdded));
            DeviceService test = new DeviceService(this.devicesMocked.Object);

            // Act
            test.AddNewDevice(deviceToBeAdded);
            var newcount = test.GetAll().Count;

            // Assert
            this.devicesMocked.Verify(x => x.AddNewDevice(deviceToBeAdded), Times.Once);
            CollectionAssert.Contains(this.devices, deviceToBeAdded);
        }

        /// <summary>
        /// Test whether we can we delete a device.
        /// </summary>
        [Test]
        public void Delete_device_Test()
        {
            // Arrange
            var deviceToBeDeleted = new Devices()
            {
                Device_ID = 1,
                Modell = "Apple Macbook",
                State_now = "Raktáron",
            };
            this.devicesMocked.Setup(x => x.Delete(deviceToBeDeleted.Device_ID)).Callback(() => this.devices.Remove(deviceToBeDeleted));
            DeviceService test = new DeviceService(this.devicesMocked.Object);

            // Act
            test.Delete(deviceToBeDeleted.Device_ID);

            // Assert
            Mock.Verify(this.devicesMocked);
            CollectionAssert.DoesNotContain(this.devices, deviceToBeDeleted);
        }

        /// <summary>
        /// Tests if we are capable of getting the correct device back upon searching via ID.
        /// </summary>
        [Test]
        public void Get_device_By_ID_Test()
        {
            // Arrange
            this.devicesMocked.Setup(x => x.GetDeviceById(1)).Returns(this.devices.First());
            var deviceToFind = new Devices()
            {
                Device_ID = 1,
                Modell = "Apple Macbook",
                State_now = "Raktáron",
            };
            DeviceService test = new DeviceService(this.devicesMocked.Object);

            // Act
            Devices found = test.GetDeviceByID(deviceToFind.Device_ID);

            // Assert
            Mock.Verify(this.devicesMocked);
            Assert.That(found.Device_ID, Is.EqualTo(deviceToFind.Device_ID));
        }

        /// <summary>
        /// Tests if we get correct data from the querry when searching for devices on storage.
        /// </summary>
        [Test]
        public void NonCrud_Get_All_Devices_On_Storage_Test()
        {
            // Arrange
            this.devices = new List<Devices>()
            {
                new Devices()
                {
                    Device_ID = 1,
                    Modell = "Apple Macbook",
                    State_now = "Felhasználónál",
                },
                new Devices()
                {
                    Device_ID = 2,
                    Modell = "Apple Macbook",
                    State_now = "Felhasználónál",
                },
                new Devices()
                {
                    Device_ID = 3,
                    Modell = "Apple Macbook",
                    State_now = "Raktáron",
                },
            };
            List<string> fake = new List<string>();
            fake.Add("Apple Macbook,2");

            this.devicesMocked.Setup(x => x.CountDeviceOnStore()).Returns(fake);

            DeviceService test = new DeviceService(this.devicesMocked.Object);

            // Act
            List<DeviceModellAndCount> returnedValue = test.CountDeviceOnStore();
            string answer = returnedValue.First().ModellOfIt + ',' + returnedValue.First().CountOf;

            // Assert
            Mock.Verify(this.devicesMocked);
            Assert.That(fake.First(), Is.EqualTo(answer));
        }

        /// <summary>
        /// Tests if we get correct data from the querry when wanting to know how many equipment we have at our users' hand.
        /// </summary>
        [Test]
        public void NonCrud_Get_All_Devices_with_Users_Test()
        {
            // Arrange
            this.devices = new List<Devices>()
            {
                new Devices()
                {
                    Device_ID = 1,
                    Modell = "Apple Macbook",
                    State_now = "Felhasználónál",
                },
                new Devices()
                {
                    Device_ID = 2,
                    Modell = "Apple Macbook",
                    State_now = "Felhasználónál",
                },
                new Devices()
                {
                    Device_ID = 3,
                    Modell = "Apple Macbook",
                    State_now = "Raktáron",
                },
            };
            List<string> fake = new List<string>();
            fake.Add("Apple Macbook,1");

            this.devicesMocked.Setup(x => x.CountDevicesUsed()).Returns(fake);

            DeviceService test = new DeviceService(this.devicesMocked.Object);

            // Act
            List<DeviceModellAndCount> returnedValue = test.CountDevicesUsed();
            string answer = returnedValue.First().ModellOfIt + ',' + returnedValue.First().CountOf;

            // Assert
            Mock.Verify(this.devicesMocked);
            Assert.That(fake.First(), Is.EqualTo(answer));
        }
    }
}
