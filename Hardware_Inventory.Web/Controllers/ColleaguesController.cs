﻿using AutoMapper;
using Hardware_Inventory.BL.Interfaces;
using Hardware_Inventory.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Hardware_Inventory.Repository.Repositories;
using Hardware_Inventory.BL.Services;
using Hardware_Inventory.Web;
using Hardware_Inventory.DA;

namespace Hardware_Inventory.Web.Views
{
    public class ColleaguesController : Controller
    {
        ColleagueService logic;
        IMapper mapper;
        ColleaguesViewModel vm;

        public ColleaguesController()
        {
            ColleaguesRepository ColleagueRepo = new ColleaguesRepository();
            logic = new ColleagueService(ColleagueRepo);
            mapper = MapperFactory.CreateMapper();

            vm = new ColleaguesViewModel();
            vm.EditedColleague = new Colleague();
            var AllColleagues = logic.GetAll();
            vm.ListOfColleagues = mapper.Map<IList<DA.colleagues>, List<Web.Models.Colleague>>(AllColleagues);

        }

        private Colleague GetColleague(int id)
        {
            colleagues oneColleague = logic.GetColleagueByID(id);
            return mapper.Map<Hardware_Inventory.DA.colleagues, Models.Colleague>(oneColleague);

        }

        // GET: Colleagues
        public ActionResult Index()
        {
            ViewData["editAction"] = "AddNew";
            return View("ColleagueIndex", vm);
        }

        // GET: Colleagues/Details/5
        public ActionResult Details(int id)
        {
            return View("ColleagueDetails",GetColleague(id));
        }

        // GET: Colleagues/Delete/5
        public ActionResult Delete(int id)
        {
            TempData["editResult"] = "Delete FAILED";
            logic.Delete(id);           
            if (logic.GetColleagueByID(id)==null) TempData["editResult"] = "Succesfully deleted";
            return RedirectToAction(nameof(Index));
        }

        // GET: Colleagues/Edit/5
        public ActionResult Edit(int id)
        {
            ViewData["editAction"] = "Edit";
            vm.EditedColleague = GetColleague(id);
            return View("ColleagueIndex", vm);
        }

        /// Post: Colleagues/Edit/5
        [HttpPost]
        public ActionResult Edit(Colleague colleague, string editAction)
        {
            if (ModelState.IsValid && colleague != null)
            {
                if (editAction == "AddNew")
                {
                    logic.AddNewColleague(new colleagues() { Full_Name = colleague.Name, Email = colleague.Mail, Post = colleague.Post, WorksAt = colleague.Workplace });
                }
                else
                {
                    if(logic.UpdateColleague(new colleagues() { Users_ID = colleague.ID, Full_Name = colleague.Name, Email = colleague.Mail, Post = colleague.Post, WorksAt = colleague.Workplace }))
                        TempData["editResult"] = "Edited Succesfully";
                    else
                        TempData["editResult"] = "Edit Failed";
                }
                return RedirectToAction(nameof(Index));
            }
            else
            {
                ViewData["editAction"] = "Edit";
                vm.EditedColleague = colleague;
                return View("ColleagueIndex", vm);
            }
        }
    }
}
