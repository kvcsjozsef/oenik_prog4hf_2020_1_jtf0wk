﻿// <copyright file="CalorieCounterController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WebApplication1.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using WebApplication1.Models;

    public class CalorieCounterController : Controller
    {
        static Random rnd = new Random();

        public List<Exercises> exercises = new List<Exercises>();

        private int CalculateCalories(int weight, int duration,string sport)
        {
            this.exercises.Add(new Exercises("Running", 1000));
            this.exercises.Add(new Exercises("Yoga", 400));
            this.exercises.Add(new Exercises("Pilates", 472));
            this.exercises.Add(new Exercises("Hiking", 700));
            this.exercises.Add(new Exercises("Swimming", 1000));
            this.exercises.Add(new Exercises("Bicycle", 600));

            var exercise = this.exercises.Find(x => x.Name.Equals(sport));
            double hour = (double) duration / 60;
            double thickboi = (double)weight / 100;

            return Convert.ToInt32(exercise.Calories*hour*thickboi);
        }

        //GET: /CalorieCounter/Calories
        public ActionResult Calories()
        {
            return View("ExerciseInput");
        }

        //POST: /CalorieCounter/Calories
        [HttpPost]
        public ActionResult Calories(ExerciseInput input)
        {
            if (!this.Request.Form.AllKeys.Contains(nameof(ExerciseInput.Name)))
            {
                this.TempData["myWarning"] = "MISSING DATA";
                return this.RedirectToAction(nameof(Calories));
            }
            if (input.Duration <= 0 || input.Weight <= 0)
            {
                this.TempData["myWarning"] = "INVALID DATA";
                return this.RedirectToAction(nameof(Calories));
            }

            ExerciseOutput er = new ExerciseOutput()
            {
                Name = input.Name,
                Weight = input.Weight,
                Sport = input.Sport,
                Duration = input.Duration,
                BurnedCalories = this.CalculateCalories(input.Weight,input.Duration,input.Sport),
            };

            return View("ExerciseOutput",er);
        }
    }
}