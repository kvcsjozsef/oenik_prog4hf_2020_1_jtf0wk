﻿using AutoMapper;
using Hardware_Inventory.BL.Interfaces;
using Hardware_Inventory.BL.Services;
using Hardware_Inventory.DA;
using Hardware_Inventory.Repository.Repositories;
using Hardware_Inventory.Web.Models;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Web.Http;

namespace Hardware_Inventory.Web.Controllers
{
    public class ColleaguesApiController : ApiController
    {
        public class ApiResult
        { 
            public bool OperationResult { get; set; }
        }

        IColleagueService logic;
        IMapper mapper;
        public ColleaguesApiController()
        {
            ColleaguesRepository ColleagueRepo = new ColleaguesRepository();
            logic = new ColleagueService(ColleagueRepo);
            mapper = MapperFactory.CreateMapper();
        }

        //GET /api/ColleaguesApi/all
        [ActionName("all")]
        [HttpGet]
        public IEnumerable<Colleague> GetAll()
#pragma warning restore CS8321 // Local function is declared but never used
        {
            var colleagues = logic.GetAll();
            return mapper.Map<IList<DA.colleagues>, List<Models.Colleague>>(colleagues);
        }

        //GET /api/ColleaguesApi/del/19
        [ActionName("del")]
        [HttpGet]
        public ApiResult DelOneColleague(int id)
        {
            bool success = false;
            logic.Delete(id);
            if (logic.GetColleagueByID(id) == null) success = true;
            return new ApiResult() { OperationResult = success };

        }
        //GET /api/ColleaguesApi/add + COLLEAGUE
        [ActionName("add")]
        [HttpPost]
        public ApiResult AddOneColleague(Colleague colleague)
        {
            logic.AddNewColleague(new colleagues() { Full_Name = colleague.Name, Email = colleague.Mail, Post = colleague.Post, WorksAt = colleague.Workplace });
            return new ApiResult() { OperationResult = true };
        }
        //GET /api/ColleaguesApi/modify + colleague
        [ActionName("modify")]
        [HttpPost]
        public ApiResult ModifyOneColleague(Colleague colleague)
        {
            bool success = logic.UpdateColleague(new colleagues() {Users_ID=colleague.ID, Full_Name = colleague.Name, Email = colleague.Mail, Post = colleague.Post, WorksAt = colleague.Workplace });
            return new ApiResult() { OperationResult = success };
        }
    }
}
