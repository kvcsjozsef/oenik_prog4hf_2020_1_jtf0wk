﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class ExerciseInput
    {
        public string Name { get; set; }

        public int Weight { get; set; }

        public int Duration { get; set; }

        public string Sport { get; set; }
    }
}