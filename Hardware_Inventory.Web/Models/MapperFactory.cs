﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hardware_Inventory.Web.Models
{
    public class MapperFactory
    {
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
          {
              cfg.CreateMap<Hardware_Inventory.DA.colleagues, Hardware_Inventory.Web.Models.Colleague>().
              ForMember(dest => dest.ID, map => map.MapFrom(src => src.Users_ID)).
              ForMember(dest => dest.Name, map => map.MapFrom(src => src.Full_Name)).
              ForMember(dest => dest.Mail, map => map.MapFrom(src => src.Email)).
              ForMember(dest => dest.Workplace, map => map.MapFrom(src => src.WorksAt)).
              ForMember(dest => dest.Post, map => map.MapFrom(src => src.Post));
          });
            return config.CreateMapper();
        }
    }
}