﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hardware_Inventory.Web.Models
{
    public class ColleaguesViewModel
    {
        public Colleague EditedColleague { get; set; }

        public List<Colleague> ListOfColleagues { get; set; }
    }
}