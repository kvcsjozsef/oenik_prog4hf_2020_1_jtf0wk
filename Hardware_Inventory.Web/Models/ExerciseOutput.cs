﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class ExerciseOutput : ExerciseInput
    {
        public int BurnedCalories { get; set; }
    }
}