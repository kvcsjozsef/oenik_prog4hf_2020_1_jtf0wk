﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class Exercises
    {
        public string Name { get; set; }

        public int Calories { get; set; }

        public Exercises(string name, int calories)
        {
            Name = name;
            Calories = calories;
        }
    }
}