﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Hardware_Inventory.Web.Models
{
    //form modell
    public class Colleague
    {
        [Display(Name = "Registrational number")]
        public int ID { get; set; }
        
        [Display(Name ="Full name of the colleague")]
        [Required]
        [StringLength(40,MinimumLength =5)]
        public string Name { get; set; }

        [Display(Name ="Mail where he can be reached, be it work or personal")]
        [Required]
        [EmailAddress]
        public string Mail { get; set; }

        [Display(Name="At which facility he is working at")]
        [Required]
        public string Workplace { get; set; }

        [Display(Name="Post of the colleague")]
        [Required]
        public string Post { get; set; }
    }
}