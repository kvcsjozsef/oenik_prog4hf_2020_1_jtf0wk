﻿// <copyright file="DeviceModellAndCount.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Hardware_Inventory.BL
{
    /// <summary>
    /// This is my own datatype for storing the returned data from the non crud querys.
    /// </summary>
    public class DeviceModellAndCount
    {
        /// <summary>
        /// Gets or sets modell of the given devices.
        /// </summary>
        public string ModellOfIt { get; set; }

        /// <summary>
        /// Gets or sets count of each modell that are on store or being used.
        /// </summary>
        public int CountOf { get; set; }
    }
}
