﻿// <copyright file="IModellService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Hardware_Inventory.BL.Interfaces
{
    using System.Collections.Generic;
    using Hardware_Inventory.DA;

    /// <summary>
    /// Service interface for DB connection.
    /// </summary>
    public interface IModellService
    {
        /// <summary>
        /// Gets Modells by ID.
        /// </summary>
        /// <param name="id">Expects and ID it can find.</param>
        /// <returns>A Modell object.</returns>
        Modell GetModellByID(string id);

        /// <summary>
        /// Gets a list of all Modells.
        /// </summary>
        /// <returns>Returns that list.</returns>
        List<Modell> GetAll();

        /// <summary>
        /// Deletes a Modell.
        /// </summary>
        /// <param name="id">Expects an ID.</param>
        void Delete(string id);

        /// <summary>
        /// Updates a Modell.
        /// </summary>
        /// <param name="modell">Expects a Modell object.</param>
        void UpdateModell(Modell modell);

        /// <summary>
        /// Adds a Modell.
        /// </summary>
        /// <param name="newModell">Expects a Modell object.</param>
        void AddNewModell(Modell newModell);
    }
}
