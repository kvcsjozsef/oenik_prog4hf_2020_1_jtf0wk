﻿// <copyright file="IDeviceService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Hardware_Inventory.BL.Interfaces
{
    using System.Collections.Generic;
    using Hardware_Inventory.DA;

    /// <summary>
    /// Interface for Device entity.
    /// </summary>
    public interface IDeviceService
    {
        /// <summary>
        /// Gets Devices by ID.
        /// </summary>
        /// <param name="id">Expects and ID it can find.</param>
        /// <returns>A Device object.</returns>
        Devices GetDeviceByID(int id);

        /// <summary>
        /// Gets a list of all Devices.
        /// </summary>
        /// <returns>Returns that list.</returns>
        List<Devices> GetAll();

        /// <summary>
        /// Deletes a Device.
        /// </summary>
        /// <param name="id">Expects an ID.</param>
        void Delete(int id);

        /// <summary>
        /// Updates a Device.
        /// </summary>
        /// <param name="device">Expects a Device object.</param>
        void UpdateDevice(Devices device);

        /// <summary>
        /// Adds a Device.
        /// </summary>
        /// <param name="device">Expects a Device object.</param>
        void AddNewDevice(Devices device);

        /// <summary>
        /// This will be a non CRUD operation. Uses Join.
        /// </summary>
        /// <returns>A list with all the Devives and their users.</returns>
        List<string> AllDevicesWithUsers();

        /// <summary>
        /// Shows that how many devices we have at our users' hand. Broken down to modell.
        /// </summary>
        /// <returns>A list that contains that information.</returns>
        List<DeviceModellAndCount> CountDevicesUsed();

        /// <summary>
        /// Shows how many devices we have on store broken down to modells.
        /// </summary>
        /// <returns>A list that contains that query result.</returns>
        List<DeviceModellAndCount> CountDeviceOnStore();
    }
}
