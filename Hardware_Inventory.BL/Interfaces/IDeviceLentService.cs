﻿// <copyright file="IDeviceLentService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Hardware_Inventory.BL.Interfaces
{
    using System.Collections.Generic;
    using Hardware_Inventory.DA;

    /// <summary>
    /// Interface for Device entity.
    /// </summary>
    public interface IDeviceLentService
    {
        /// <summary>
        /// Gets DeviceLents by User ID.
        /// </summary>
        /// <param name="id">Expects and ID it can find.</param>
        /// <returns>A DeviceLent object.</returns>
        DeviceLent GetDeviceLentByUserID(int id);

        /// <summary>
        /// Gets DeviceLents by Device ID.
        /// </summary>
        /// <param name="id">Expects and ID it can find.</param>
        /// <returns>A DeviceLent object.</returns>
        DeviceLent GetDeviceLentByDeviceID(int id);

        /// <summary>
        /// Gets a list of all DeviceLents.
        /// </summary>
        /// <returns>Returns that list.</returns>
        List<DeviceLent> GetAll();

        /// <summary>
        /// Deletes a DeviceLent.
        /// </summary>
        /// <param name="userid">Expects a userID it can find.</param>
        /// <param name="deviceid">Expect a DeviceID it can find.</param>
        void Delete(int userid, int deviceid);

        /// <summary>
        /// Updates a DeviceLent.
        /// </summary>
        /// <param name="deviceLent">Expects a DeviceLent object.</param>
        void UpdateDeviceLent(DeviceLent deviceLent);

        /// <summary>
        /// Adds a DeviceLent.
        /// </summary>
        /// <param name="deviceLent">Expects a DeviceLent object.</param>
        void AddNewDeviceLent(DeviceLent deviceLent);
    }
}
