﻿// <copyright file="IColleagueService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Hardware_Inventory.BL.Interfaces
{
    using System.Collections.Generic;
    using Hardware_Inventory.DA;

    /// <summary>
    /// Interface for Colleague entity.
    /// </summary>
    public interface IColleagueService
    {
        /// <summary>
        /// Gets Colleagues by ID.
        /// </summary>
        /// <param name="id">Expects and ID it can find.</param>
        /// <returns>A Colleague object.</returns>
        colleagues GetColleagueByID(int id);

        /// <summary>
        /// Gets a list of all Colleagues.
        /// </summary>
        /// <returns>Returns that list.</returns>
        List<colleagues> GetAll();

        /// <summary>
        /// Deletes a Colleague.
        /// </summary>
        /// <param name="id">Expects a ID.</param>
        void Delete(int id);

        /// <summary>
        /// Updates a Colleague.
        /// </summary>
        /// <param name="colleague">Expects a Colleague object with existing ID.</param>
        bool UpdateColleague(colleagues colleague);

        /// <summary>
        /// Adds a Colleague.
        /// </summary>
        /// <param name="colleague">Expects a new Colleague object.</param>
        void AddNewColleague(colleagues colleague);
    }
}
