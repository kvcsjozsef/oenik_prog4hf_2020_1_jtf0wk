﻿// <copyright file="NewDeviceFromWeb.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Hardware_Inventory.BL
{
    using System.Net.Http;
    using System.Text;
    using Hardware_Inventory.DA;
    using Newtonsoft.Json;

    /// <summary>
    /// Requests a new device config from the Java Endpoint, the User only has to Enter a modell type.
    /// </summary>
    public class NewDeviceFromWeb
    {
        /// <summary>
        /// URL for the connection.
        /// </summary>
        private const string Url = "http://localhost:8080/NewDevice/NewDeviceServlet";

        /// <summary>
        /// Creating the client.
        /// </summary>
        private HttpClient connection = new HttpClient();

        /// <summary>
        /// Does the work. This function asks the java endpoint for the config and returns as a Modell object.
        /// </summary>
        /// <param name="input">Expects a modell name.</param>
        /// <returns>Returns a config with that given modell name.</returns>
        public Modell GetNewModell(string input)
        {
            string message = JsonConvert.SerializeObject(new
            {
                input,
            });

            StringContent toBeSent = new StringContent(message, Encoding.UTF8);

            var answer = this.connection.PostAsync(Url, toBeSent).Result;

            string jsonAnswer = answer.Content.ReadAsStringAsync().Result;

            return JsonConvert.DeserializeObject<Modell>(jsonAnswer);
            }
    }
}
