﻿// <copyright file="ModellService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Hardware_Inventory.BL.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using Hardware_Inventory.BL.Interfaces;
    using Hardware_Inventory.DA;
    using Hardware_Inventory.Repository.Interfaces;
    using Hardware_Inventory.Repository.Repositories;

    /// <summary>
    /// Service for Modell entity.
    /// </summary>
    public class ModellService : IModellService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ModellService"/> class.
        /// Default construct auto-generated.
        /// </summary>
        public ModellService()
        {
            this.ModellRepository = new ModellRepository();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ModellService"/> class.
        /// For moqyou purposes.
        /// </summary>
        /// <param name="repository">Expects a repository that is moq-ed.</param>
        public ModellService(IModellRepository repository)
        {
            this.ModellRepository = repository;
        }

        /// <summary>
        /// Gets or sets a new instance of the <see cref="ModellService"/> class.
        /// Default construct auto-generated.
        /// </summary>
        private IModellRepository ModellRepository { get; set; }

        /// <inheritdoc/>
        public void AddNewModell(Modell newModell)
        {
            this.ModellRepository.AddNewModell(newModell);
        }

        /// <inheritdoc/>
        public void Delete(string id)
        {
            this.ModellRepository.Delete(id);
        }

        /// <inheritdoc/>
        public List<Modell> GetAll()
        {
            return this.ModellRepository.GetAll().ToList();
        }

        /// <inheritdoc/>
        public Modell GetModellByID(string id)
        {
            return this.ModellRepository.GetModellById(id);
        }

        /// <inheritdoc/>
        public void UpdateModell(Modell modell)
        {
            this.ModellRepository.UpdateModell(modell);
        }
    }
}
