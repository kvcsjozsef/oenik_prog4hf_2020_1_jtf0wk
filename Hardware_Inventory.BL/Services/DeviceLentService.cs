﻿// <copyright file="DeviceLentService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Hardware_Inventory.BL.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using Hardware_Inventory.BL.Interfaces;
    using Hardware_Inventory.DA;
    using Hardware_Inventory.Repository.Interfaces;
    using Hardware_Inventory.Repository.Repositories;

    /// <summary>
    /// Service for DeviceLent entity.
    /// </summary>
    public class DeviceLentService : IDeviceLentService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DeviceLentService"/> class.
        /// Constructor for actual purposes.
        /// </summary>
        public DeviceLentService()
        {
            this.DeviceLentRepository = new DeviceLentRepository();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DeviceLentService"/> class.
        /// For moqyou purposes.
        /// </summary>
        /// <param name="repository">Expects a repository that is moq-ed.</param>
        public DeviceLentService(IDeviceLentRepository repository)
        {
            this.DeviceLentRepository = repository;
        }

        private IDeviceLentRepository DeviceLentRepository { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="DeviceLentService"/> class.
        /// Default construct auto-generated.
        /// </summary>

        /// <inheritdoc/>
        /// <param name="deviceLent">Expects a DeviceLent object.</param>
        public void AddNewDeviceLent(DeviceLent deviceLent)
        {
            this.DeviceLentRepository.AddNewDeviceLent(deviceLent);
        }

        /// <inheritdoc/>
        public void Delete(int userid, int deviceid)
        {
            this.DeviceLentRepository.Delete(userid, deviceid);
        }

        /// <inheritdoc/>
        public List<DeviceLent> GetAll()
        {
            return this.DeviceLentRepository.GetAll().ToList();
        }

        /// <inheritdoc/>
        public void UpdateDeviceLent(DeviceLent deviceLent)
        {
            this.DeviceLentRepository.UpdateDeviceLent(deviceLent);
        }

        /// <inheritdoc/>
        public DeviceLent GetDeviceLentByUserID(int id)
        {
            return this.DeviceLentRepository.GetDeviceLentByUserId(id);
        }

        /// <inheritdoc/>
        public DeviceLent GetDeviceLentByDeviceID(int id)
        {
            return this.DeviceLentRepository.GetDeviceLentByDeviceId(id);
        }
    }
}
