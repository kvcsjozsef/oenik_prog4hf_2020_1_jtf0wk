﻿// <copyright file="ColleagueService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Hardware_Inventory.BL.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using Hardware_Inventory.BL.Interfaces;
    using Hardware_Inventory.DA;
    using Hardware_Inventory.Repository;
    using Hardware_Inventory.Repository.Repositories;

    /// <summary>
    /// Service for Colleague entity.
    /// </summary>
    public class ColleagueService : IColleagueService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ColleagueService"/> class.
        /// Construcotr for the actual use.
        /// </summary>
        public ColleagueService()
        {
            this.ColleagueRepository = new ColleaguesRepository();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ColleagueService"/> class.
        /// For moqyou purposes.
        /// </summary>
        /// <param name="repository">Expects a repository that is moq-ed.</param>
        public ColleagueService(IColleaguesRepository repository)
        {
            this.ColleagueRepository = repository;
        }

        /// <summary>
        /// Gets or sets a new instance of the <see cref="ColleagueService"/> class.
        /// Default construct auto-generated.
        /// </summary>
        private IColleaguesRepository ColleagueRepository { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ColleagueService"/> class.
        /// Default construct auto-generated.
        /// </summary>

        /// <inheritdoc/>
        public void AddNewColleague(colleagues colleague)
        {
            this.ColleagueRepository.AddNewColleague(colleague);
        }

        /// <inheritdoc/>
        public void Delete(int id)
        {
            this.ColleagueRepository.Delete(id);
        }

        /// <inheritdoc/>
        public List<colleagues> GetAll()
        {
            return this.ColleagueRepository.GetAll().ToList();
        }

        /// <inheritdoc/>
        public colleagues GetColleagueByID(int id)
        {
            return this.ColleagueRepository.GetColleagueById(id);
        }

        /// <inheritdoc/>
        public bool UpdateColleague(colleagues colleague)
        {
            return this.ColleagueRepository.UpdateColleague(colleague);
        }
    }
}
