﻿// <copyright file="DeviceService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Hardware_Inventory.BL.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using Hardware_Inventory.BL.Interfaces;
    using Hardware_Inventory.DA;
    using Hardware_Inventory.Repository.Interfaces;
    using Hardware_Inventory.Repository.Repositories;

    /// <summary>
    /// Service for Device entity.
    /// </summary>
    public class DeviceService : IDeviceService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DeviceService"/> class.
        /// For moqyou purposes.
        /// </summary>
        public DeviceService()
        {
            this.DeviceRepository = new DevicesRepository();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DeviceService"/> class.
        /// For moqyou purposes.
        /// </summary>
        /// <param name="repository">Expects a repository that can be used for mock purposes.</param>
        public DeviceService(IDevicesRepository repository)
        {
            this.DeviceRepository = repository;
        }

        private IDevicesRepository DeviceRepository { get; set; }

        /// <inheritdoc/>
        public void AddNewDevice(Devices device)
        {
            this.DeviceRepository.AddNewDevice(device);
        }

        /// <inheritdoc/>
        public List<string> AllDevicesWithUsers()
        {
            return this.DeviceRepository.GetAllDeviceWithUser();
        }

        /// <inheritdoc/>
        public List<DeviceModellAndCount> CountDevicesUsed()
        {
            List<string> current = this.DeviceRepository.CountDevicesUsed();
            List<DeviceModellAndCount> answer = new List<DeviceModellAndCount>();
            foreach (var a in current)
            {
                string[] data = a.Split(',');
                answer.Add(new DeviceModellAndCount { ModellOfIt = data[0], CountOf = int.Parse(data[1]) });
            }
            return answer;
        }

        /// <inheritdoc/>
        public List<DeviceModellAndCount> CountDeviceOnStore()
        {
            List<string> current = this.DeviceRepository.CountDeviceOnStore();
            List<DeviceModellAndCount> answer = new List<DeviceModellAndCount>();
            foreach (var a in current)
            {
                string[] data = a.Split(',');
                answer.Add(new DeviceModellAndCount { ModellOfIt = data[0], CountOf = int.Parse(data[1]) });
            }
            return answer;
        }

        /// <inheritdoc/>
        public void Delete(int id)
        {
            this.DeviceRepository.Delete(id);
        }

        /// <inheritdoc/>
        public List<Devices> GetAll()
        {
            return this.DeviceRepository.GetAll().ToList();
        }

        /// <inheritdoc/>
        public Devices GetDeviceByID(int id)
        {
            return this.DeviceRepository.GetDeviceById(id);
        }

        /// <inheritdoc/>
        public void UpdateDevice(Devices device)
        {
            this.DeviceRepository.UpdateDevice(device);
        }
    }
}
