﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Hardware_Inventory
{
    using System;
    using System.Collections.Generic;
    using Hardware_Inventory.BL;
    using Hardware_Inventory.BL.Services;
    using Hardware_Inventory.DA;

    /// <summary>
    /// The main program. Does the UI part of the job.
    /// </summary>
    public class Program
    {
        private static DeviceService devices = new DeviceService();
        private static DeviceLentService devicelent = new DeviceLentService();
        private static ModellService modells = new ModellService();
        private static ColleagueService colleague = new ColleagueService();

        /// <summary>
        /// The entry point of the program.
        /// </summary>
        /// <param name="args">Can be given paramaters but can't use them.</param>
        public static void Main(string[] args)
        {
            while (true)
            {
                MainMenu();
                Console.ReadKey();
            }
        }

        private static void MainMenu()
        {
            Console.WriteLine("1-I want do some CRUD");
            Console.WriteLine("2-I want to order a new config form the web");
            Console.WriteLine("3-I Want to list all the equipment with the users attached to them");
            Console.WriteLine("4-I want to know how many devices do we have our users' hand broken down to modells");
            Console.WriteLine("5-I want to see how many device we have on store,grouped by modell");
            string input = Console.ReadLine();
            switch (input)
            {
                case "1":
                    Console.Clear();
                    TablePicker();
                    break;
                case "2":
                    Console.Clear();
                    OrderNewDevice();
                    break;
                case "3":
                    Console.Clear();
                    GetAllDevicesWithUser();
                    break;
                case "4":
                    Console.Clear();
                    CountDevicesUsed();
                    break;
                case "5":
                    Console.Clear();
                    CountDevicesOnStore();
                    break;
                default:
                    Console.Clear();
                    Console.WriteLine("Please type something that I can understand. For example a number from the list below:");
                    break;
            }
        }

        private static void TablePicker()
        {
            Console.WriteLine("Pick the table you wish to do some crud on");
            Console.WriteLine("1-Colleagues");
            Console.WriteLine("2-Devices");
            Console.WriteLine("3-Modells");
            Console.WriteLine("4-Devicelent (only via Device ID)");
            Console.WriteLine("Anything else to cancel the operation");
            string input = Console.ReadLine();
            if (int.Parse(input) >= 1 && int.Parse(input) <= 4)
            {
                Console.Clear();
                OperationPicker(input);
            }
        }

        private static void OperationPicker(string table)
        {
            Console.WriteLine("Pick the table you wish to do some crud on");
            Console.WriteLine("1-Add");
            Console.WriteLine("2-List all");
            Console.WriteLine("3-Delete");
            Console.WriteLine("4-Update");
            Console.WriteLine("Anything else to cancel the operation");
            string input = Console.ReadLine();
            if (int.Parse(input) >= 1 && int.Parse(input) <= 4)
            {
                Console.Clear();
                Picker(table, input);
            }
        }

        private static void Picker(string table, string operation)
        {
            switch (table)
            {
                case "1":
                    switch (operation)
                    {
                        case "1":
                            Console.WriteLine("Please fill this like this:Full_Name,Email,WorksAt,Post");
                            string input = Console.ReadLine();
                            try
                            {
                                string[] adata = input.Split(',');
                                colleague.AddNewColleague(new colleagues() { Full_Name = adata[1], Email = adata[2], WorksAt = adata[3], Post = adata[4] });
                            }
                            catch
                            {
                                Console.WriteLine("I can't work with that. start over.");
                                Console.ReadKey();
                            }

                            break;
                        case "2":
                            List<colleagues> data = colleague.GetAll();
                            foreach (var item in data)
                            {
                                Console.WriteLine($"{item.Users_ID} - {item.Full_Name} - {item.Email} - {item.WorksAt} - {item.Post}");
                            }

                            break;
                        case "3":
                            Console.WriteLine("Please give me an ID I can search for");
                            string todelete = Console.ReadLine();
                            bool succesfullConversion = int.TryParse(todelete, out int deletethisindex);
                            if (succesfullConversion)
                            {
                                try
                                {
                                    colleague.Delete(deletethisindex);
                                }
                                catch
                                {
                                    Console.WriteLine("Index not found");
                                }
                            }

                            break;
                        case "4":
                            Console.WriteLine("Type the ID of the Colleague you want to update(bear in mind you have to reenter every data of them)");
                            Console.WriteLine("If the Id matches it will be updated, else you'll have to start over");
                            Console.WriteLine("Type it like this:User_ID,Full_Name,Email,WorksAt,Post");
                            string datatoconvert = Console.ReadLine();
                            try
                            {
                                string[] adata = datatoconvert.Split(',');
                                colleague.UpdateColleague(new colleagues() { Users_ID = int.Parse(adata[0]), Full_Name = adata[1], Email = adata[2], WorksAt = adata[3], Post = adata[4] });
                                Console.WriteLine("Table updated!");
                            }
                            catch
                            {
                                Console.WriteLine("I can't work with that. start over.");
                                Console.ReadKey();
                            }

                            break;
                    }

                    break;
                case "2":
                    switch (operation)
                    {
                        case "1":
                            Console.WriteLine("Please fill this like this:Modell,Comment");
                            Console.WriteLine("It will be automatically put into the storage space and the last maint will be the current date");
                            string input = Console.ReadLine();
                            try
                            {
                                string[] adata = input.Split(',');
                                devices.AddNewDevice(new Devices() { Modell = adata[1], Last_Maint = DateTime.Now, Comments = adata[2], State_now = "Raktáron" });
                            }
                            catch
                            {
                                Console.WriteLine("I can't work with that. start over.");
                                Console.ReadKey();
                            }

                            break;
                        case "2":
                            var data = devices.GetAll();
                            foreach (var item in data)
                            {
                                Console.WriteLine($"{item.Device_ID} - {item.Modell} - {item.State_now} - {item.Last_Maint} - {item.Comments}");
                            }

                            break;
                        case "3":
                            Console.WriteLine("Please give me an ID I can search for");
                            string todelete = Console.ReadLine();
                            bool succesfullConversion = int.TryParse(todelete, out int deletethisindex);
                            if (succesfullConversion)
                            {
                                try
                                {
                                    devices.Delete(deletethisindex);
                                }
                                catch
                                {
                                    Console.WriteLine("Index not found or other error occured");
                                }
                            }

                            break;
                        case "4":
                            Console.WriteLine("Type the ID of the Device you want to update(bear in mind you have to reenter every data of them)");
                            Console.WriteLine("If the Id matches it will be updated, else you'll have to start over");
                            Console.WriteLine("The only accepted date format is: YYYY.MM.DD HH:mm:SS - if it is not given right you will return to the main screen");
                            Console.WriteLine("Type it like this:Device_ID,Modell,State_Now,Last_Maint,Comment");
                            string datatoconvert = Console.ReadLine();
                            try
                            {
                                string[] adata = datatoconvert.Split(',');
                                devices.UpdateDevice(new Devices() { Device_ID = int.Parse(adata[0]), Modell = adata[1], State_now = adata[2], Last_Maint = DateTime.Parse(adata[3]), Comments = adata[4] });
                                Console.WriteLine("Table updated!");
                            }
                            catch
                            {
                                Console.WriteLine("I can't work with that. start over.");
                                Console.ReadKey();
                            }

                            break;
                    }

                    break;
                case "3":
                    switch (operation)
                    {
                        case "1":
                            Console.WriteLine("Please fill this like this:Modell,DeviceType,Cpu,Ram size,Storage Type, Storage size");
                            string input = Console.ReadLine();
                            try
                            {
                                string[] adata = input.Split(',');
                                modells.AddNewModell(new Modell() { Modell_ID = adata[0], DeviceType = adata[1], CPU = adata[2], RAM = int.Parse(adata[3]), Storage_type = adata[4], Storage_Size = int.Parse(adata[5]) });
                            }
                            catch
                            {
                                Console.WriteLine("I can't work with that. start over.");
                                Console.ReadKey();
                            }

                            break;
                        case "2":
                            var data = modells.GetAll();
                            foreach (var item in data)
                            {
                                Console.WriteLine($"{item.Modell_ID} - {item.DeviceType} - {item.CPU} - {item.RAM} - {item.Storage_Size} - {item.Storage_type}");
                            }

                            break;
                        case "3":
                            Console.WriteLine("Please give me an exact modell name I can search for");
                            string todelete = Console.ReadLine();
                            try
                            {
                                modells.Delete(todelete);
                            }
                            catch
                            {
                                Console.WriteLine("Index not found or other error occured");
                            }

                            break;
                        case "4":
                            Console.WriteLine("Type the name of the Modell you want to update(bear in mind you have to reenter every data of them)");
                            Console.WriteLine("If the Id matches it will be updated, else you'll have to start over");
                            Console.WriteLine("Type it like this:Modell,Devicetype,CPU,RAM,Storagetype,Storagesize");
                            string datatoconvert = Console.ReadLine();
                            try
                            {
                                string[] adata = datatoconvert.Split(',');
                                modells.UpdateModell(new Modell() { Modell_ID = adata[0], DeviceType = adata[1], CPU = adata[2], RAM = int.Parse(adata[3]), Storage_type = adata[4], Storage_Size = int.Parse(adata[5]) });
                                Console.WriteLine("Table updated!");
                            }
                            catch
                            {
                                Console.WriteLine("I can't work with that. start over.");
                                Console.ReadKey();
                            }

                            break;
                    }

                    break;
                case "4":
                    switch (operation)
                    {
                        case "1":
                            Console.WriteLine("The date will be automatically added");
                            Console.WriteLine("Please fill this like this:Device_Id,User_ID");
                            string input = Console.ReadLine();
                            try
                            {
                                string[] adata = input.Split(',');
                                devicelent.AddNewDeviceLent(new DeviceLent() { Device_ID = int.Parse(adata[0]), Users_ID = int.Parse(adata[1]), Lent_on = DateTime.Now });
                            }
                            catch
                            {
                                Console.WriteLine("I can't work with that. start over.");
                                Console.ReadKey();
                            }

                            break;
                        case "2":
                            var data = devicelent.GetAll();
                            foreach (var item in data)
                            {
                                Console.WriteLine($"{item.Device_ID} - {item.Users_ID} - {item.Lent_on}");
                            }

                            break;
                        case "3":
                            Console.WriteLine("Please give me an exact device id and a user id separated by a \",\" -comma");
                            string[] todelete = Console.ReadLine().Split(',');
                            try
                            {
                                devicelent.Delete(int.Parse(todelete[0]), int.Parse(todelete[1]));
                            }
                            catch
                            {
                                Console.WriteLine("Index not found or other error occured");
                            }

                            break;
                        case "4":
                            Console.WriteLine("Type the name of the Modell you want to update(bear in mind you have to reenter every data of them)");
                            Console.WriteLine("If the Id matches it will be updated, else you'll have to start over");
                            Console.WriteLine("Type it like this:Modell,Devicetype,CPU,RAM,Storagetype,Storagesize");
                            string datatoconvert = Console.ReadLine();
                            try
                            {
                                string[] adata = datatoconvert.Split(',');
                                devicelent.UpdateDeviceLent(new DeviceLent() { Users_ID = int.Parse(adata[0]), Device_ID = int.Parse(adata[1]), Lent_on = DateTime.Now });
                                Console.WriteLine("Table updated!");
                            }
                            catch
                            {
                                Console.WriteLine("I can't work with that. start over.");
                                Console.ReadKey();
                            }

                            break;
                    }

                    break;
            }
        }

        private static void OrderNewDevice()
        {
            Console.WriteLine("Please tell me what brand and modell the new laptop you want to buy");
            NewDeviceFromWeb order = new NewDeviceFromWeb();
            Modell answer = order.GetNewModell(Console.ReadLine());
            Console.WriteLine("The config they offered");
            Console.WriteLine(answer.ToString());
            Console.WriteLine("Do you accept it? Y/N");
            string input = Console.ReadLine();
            while (input != "Y" || input != "N" || input != "y" || input != "n")
            {
                Console.WriteLine(answer.ToString());
                Console.WriteLine("Please answer with Y/N or y/n");
                input = Console.ReadLine();
            }

            Console.WriteLine("How many you wish to buy?");
            bool validInput = int.TryParse(Console.ReadLine(), out int pieces);
            if (!validInput)
            {
                Console.WriteLine("Beacause you entered an invalid input I have ordered 999 of them. :)");
                for (int i = 0; i < 999; i++)
                {
                    devices.AddNewDevice(new Devices() { Modell = answer.Modell_ID, Last_Maint = DateTime.Now, State_now = "Raktáron", Comments = "New" });
                }
            }
            else
            {
                for (int i = 0; i < pieces; i++)
                {
                    devices.AddNewDevice(new Devices() { Modell = answer.Modell_ID, Last_Maint = DateTime.Now, State_now = "Raktáron", Comments = "New" });
                }

                Console.WriteLine("Ordering done");
            }
        }

        private static void GetAllDevicesWithUser()
        {
            List<string> data = devices.AllDevicesWithUsers();
            foreach (var a in data)
            {
                Console.WriteLine(a);
            }
        }

        private static void CountDevicesOnStore()
        {
            List<DeviceModellAndCount> data = devices.CountDeviceOnStore();
            foreach (var a in data)
            {
                Console.WriteLine($"We have got {a.CountOf} pieces of {a.ModellOfIt} on storage.");
            }
        }

        private static void CountDevicesUsed()
        {
            List<DeviceModellAndCount> data = devices.CountDevicesUsed();
            foreach (var a in data)
            {
                Console.WriteLine($"{a.CountOf} pieces of {a.ModellOfIt} are being used currently.");
            }
        }
    }
}
