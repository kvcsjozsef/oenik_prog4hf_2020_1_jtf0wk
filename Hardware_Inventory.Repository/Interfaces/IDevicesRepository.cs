﻿// <copyright file="IDevicesRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Hardware_Inventory.Repository.Interfaces
{
    using System.Collections.Generic;
    using System.Linq;
    using Hardware_Inventory.DA;

    /// <summary>
    /// Interface for Device Repository.
    /// </summary>
    public interface IDevicesRepository
    {
        /// <summary>
        /// Gets all Devices.
        /// </summary>
        /// <returns>Returns All Devices.</returns>
        IQueryable<Devices> GetAll();

        /// <summary>
        /// Gets Device by ID.
        /// </summary>
        /// <param name="id">Expects an ID.</param>
        /// <returns>Returns element with matching ID.</returns>
        Devices GetDeviceById(int id);

        /// <summary>
        /// Adds a new Device.
        /// </summary>
        /// <param name="newDevice">Expects a new Device to be added.</param>
        void AddNewDevice(Devices newDevice);

        /// <summary>
        /// Updates data of a Device.
        /// </summary>
        /// <param name="device">Needs data for the Device to update it.</param>
        void UpdateDevice(Devices device);

        /// <summary>
        /// Deletes by ID.
        /// </summary>
        /// <param name="id">Expects an ID it can find.</param>
        void Delete(int id);

        /// <summary>
        /// Gets returns a list with all the devices while also showing the user of that device.Only shows devices with users.
        /// </summary>
        /// <returns>A list that contains all the devies with the user beside.</returns>
        List<string> GetAllDeviceWithUser();

        /// <summary>
        /// Shows that how many devices we have at our users' hand. Broken down to modell.
        /// </summary>
        /// <returns>A list that contains that information.</returns>
        List<string> CountDevicesUsed();

        /// <summary>
        /// Shows how many devices we have on store broken down to modells.
        /// </summary>
        /// <returns>A list that contains that query result.</returns>
        List<string> CountDeviceOnStore();
    }
}
