﻿// <copyright file="IColleaguesRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Hardware_Inventory.Repository
{
    using System.Linq;
    using Hardware_Inventory.DA;

    /// <summary>
    /// Colleagues repository interface.
    /// </summary>
    public interface IColleaguesRepository
    {
        /// <summary>
        /// Gets all colleagues.
        /// </summary>
        /// <returns>Returns All colleagues.</returns>
        IQueryable<colleagues> GetAll();

        /// <summary>
        /// Deletes by ID.
        /// </summary>
        /// <param name="id">Expects an ID it can find.</param>
        void Delete(int id);

        /// <summary>
        /// Adds a new colleague.
        /// </summary>
        /// <param name="newColleague">Expects a new colleague to be added.</param>
        void AddNewColleague(colleagues newColleague);

        /// <summary>
        /// Gets colleague by ID.
        /// </summary>
        /// <param name="id">Expects an ID.</param>
        /// <returns>Returns element with matching ID.</returns>
        colleagues GetColleagueById(int id);

        /// <summary>
        /// Updates data of a colleague.
        /// </summary>
        /// <param name="colleague">Needs data for the colleague to update it.</param>
        bool UpdateColleague(colleagues colleague);
    }
}
