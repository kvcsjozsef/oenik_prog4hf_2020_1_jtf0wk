﻿// <copyright file="IDeviceLentRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Hardware_Inventory.Repository.Interfaces
{
    using System.Linq;
    using Hardware_Inventory.DA;

    /// <summary>
    /// Interface for DeviceLent repo.
    /// </summary>
    public interface IDeviceLentRepository
    {
        /// <summary>
        /// Gets all DeviceLents.
        /// </summary>
        /// <returns>Returns All DeviceLents.</returns>
        IQueryable<DeviceLent> GetAll();

        /// <summary>
        /// Gets DeviceLent by userID.
        /// </summary>
        /// <param name="userid">Expects an ID.</param>
        /// <returns>Returns element with matching ID.</returns>
        DeviceLent GetDeviceLentByUserId(int userid);

        /// <summary>
        /// Gets DeviceLent by deviceID.
        /// </summary>
        /// <param name="deviceid">Expects an ID.</param>
        /// <returns>Returns element with matching ID.</returns>
        DeviceLent GetDeviceLentByDeviceId(int deviceid);

        /// <summary>
        /// Adds a new DeviceLent.
        /// </summary>
        /// <param name="newDeviceLent">Expects a new DeviceLent to be added.</param>
        void AddNewDeviceLent(DeviceLent newDeviceLent);

        /// <summary>
        /// Updates data of a DeviceLent.
        /// </summary>
        /// <param name="deviceLent">Needs data for the DeviceLent to update it.</param>
        void UpdateDeviceLent(DeviceLent deviceLent);

        /// <summary>
        /// Deletes by user ID.
        /// </summary>
        /// <param name="userid">Expects an device ID it can find.</param>
        /// <param name="deviceid">Expects a user ID it can find.</param>
        void Delete(int userid, int deviceid);
    }
}
