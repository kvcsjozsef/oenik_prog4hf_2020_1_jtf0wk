﻿// <copyright file="IModellRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Hardware_Inventory.Repository.Interfaces
{
    using System.Linq;
    using Hardware_Inventory.DA;

    /// <summary>
    /// Interface for Modell Repo.
    /// </summary>
    public interface IModellRepository
    {
        /// <summary>
        /// Gets all Modells.
        /// </summary>
        /// <returns>Returns All Modells.</returns>
        IQueryable<Modell> GetAll();

        /// <summary>
        /// Gets Modell by ID.
        /// </summary>
        /// <param name="id">Expects an ID.</param>
        /// <returns>Returns element with matching ID.</returns>
        Modell GetModellById(string id);

        /// <summary>
        /// Adds a new Modell.
        /// </summary>
        /// <param name="newModell">Expects a new Modell to be added.</param>
        void AddNewModell(Modell newModell);

        /// <summary>
        /// Updates data of a Modell.
        /// </summary>
        /// <param name="modell">Needs data for the Modell to update it.</param>
        void UpdateModell(Modell modell);

        /// <summary>
        /// Deletes by ID.
        /// </summary>
        /// <param name="id">Expects an ID it can find.</param>
        void Delete(string id);
    }
}
