﻿// <copyright file="ColleaguesRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Hardware_Inventory.Repository.Repositories
{
    using System;
    using System.Linq;
    using Hardware_Inventory.DA;

    /// <summary>
    /// Implement the IColleaguesRepository and basic CRUD operations.
    /// </summary>
    public class ColleaguesRepository : IColleaguesRepository
    {
        private HardwareInventory db = new HardwareInventory();

        /// <inheritdoc/>
        public void AddNewColleague(colleagues newcolleague)
        {
            this.db.colleagues.Add(newcolleague);
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void Delete(int id)
        {
            var toBeDeleted = this.GetAll().Single(x => x.Users_ID == id);
            this.db.colleagues.Remove(toBeDeleted);
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public IQueryable<colleagues> GetAll()
        {
            return this.db.colleagues;
        }

        /// <inheritdoc/>
        public colleagues GetColleagueById(int id)
        {
            try
            {
                return this.GetAll().Single(x => x.Users_ID == id);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        /// <inheritdoc/>
        public bool UpdateColleague(colleagues colleague)
        {
            colleagues entity = this.GetColleagueById(colleague.Users_ID);
            if (entity == null)
            {
                return false;
            }

            entity.Full_Name = colleague.Full_Name;
            entity.Email = colleague.Email;
            entity.Post = colleague.Post;
            entity.WorksAt = colleague.WorksAt;
            db.SaveChanges();
            return true;
        }
    }
}
