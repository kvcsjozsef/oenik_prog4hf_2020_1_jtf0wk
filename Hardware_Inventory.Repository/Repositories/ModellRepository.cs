﻿// <copyright file="ModellRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Hardware_Inventory.Repository.Repositories
{
    using System.Linq;
    using Hardware_Inventory.DA;
    using Hardware_Inventory.Repository.Interfaces;

    /// <summary>
    /// Implement the IModellRepository interface.
    /// </summary>
    public class ModellRepository : IModellRepository
    {
        private HardwareInventory db = new HardwareInventory();

        /// <inheritdoc/>
        public void AddNewModell(Modell newModell)
        {
            this.db.Modell.Add(newModell);
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void Delete(string id)
        {
            var toBeDeleted = this.GetAll().Single(x => x.Modell_ID == id);
            this.db.Modell.Remove(toBeDeleted);
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public IQueryable<Modell> GetAll()
        {
            return this.db.Modell;
        }

        /// <inheritdoc/>
        public Modell GetModellById(string id)
        {
            return this.GetAll().Single(x => x.Modell_ID == id);
        }

        /// <inheritdoc/>
        public void UpdateModell(Modell modell)
        {
            this.db.Modell.Remove(this.GetModellById((string)modell.Modell_ID));
            this.db.SaveChanges();
            this.db.Modell.Add(modell);
            this.db.SaveChanges();
        }
    }
}
