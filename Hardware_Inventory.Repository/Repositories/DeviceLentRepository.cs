﻿// <copyright file="DeviceLentRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Hardware_Inventory.Repository.Repositories
{
    using System.Linq;
    using Hardware_Inventory.DA;
    using Hardware_Inventory.Repository.Interfaces;

    /// <summary>
    /// Implements the IDeviceLentRepository.
    /// </summary>
    public class DeviceLentRepository : IDeviceLentRepository
    {
        private HardwareInventory db = new HardwareInventory();

        /// <inheritdoc/>
        public void AddNewDeviceLent(DeviceLent newDeviceLent)
        {
            this.db.DeviceLent.Add(newDeviceLent);
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void Delete(int userid, int deviceid)
        {
            var toBeDeleted = this.GetAll().Single(x => x.Users_ID == userid && x.Device_ID == deviceid);
            this.db.DeviceLent.Remove(toBeDeleted);
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public IQueryable<DeviceLent> GetAll()
        {
            return this.db.DeviceLent;
        }

        /// <inheritdoc/>
        public DeviceLent GetDeviceLentByUserId(int userid)
        {
            return this.GetAll().Single(x => x.Users_ID == userid);
        }

        /// <inheritdoc/>
        public DeviceLent GetDeviceLentByDeviceId(int deviceid)
        {
            return this.GetAll().Single(x => x.Device_ID == deviceid);
        }

        /// <inheritdoc/>
        public void UpdateDeviceLent(DeviceLent deviceLent)
        {
            this.db.DeviceLent.Remove(this.GetDeviceLentByDeviceId((int)deviceLent.Users_ID));
            this.db.SaveChanges();
            this.db.DeviceLent.Add(deviceLent);
            this.db.SaveChanges();
        }
    }
}
