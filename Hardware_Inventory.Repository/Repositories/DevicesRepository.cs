﻿// <copyright file="DevicesRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Hardware_Inventory.Repository.Repositories
{
    using System.Collections.Generic;
    using System.Linq;
    using Hardware_Inventory.DA;
    using Hardware_Inventory.Repository.Interfaces;

    /// <summary>
    /// Implements the IDevicesRepository and basic CRUD operations.
    /// </summary>
    public class DevicesRepository : IDevicesRepository
    {
        private HardwareInventory db = new HardwareInventory();

        /// <inheritdoc/>
        public List<string> GetAllDeviceWithUser()
        {
            var q = from d in this.db.Devices
                    join dl in this.db.DeviceLent on d.Device_ID equals dl.Device_ID
                    join u in this.db.colleagues on dl.Users_ID equals u.Users_ID
                    select new
                    {
                        d.Device_ID,
                        d.Modell,
                        d.Last_Maint,
                        u.Users_ID,
                        u.Full_Name,
                        u.Email,
                    };
            List<string> returnable = new List<string>();
            foreach (var a in q)
            {
                returnable.Add($"{a.Device_ID} - {a.Modell} was last maintained: {a.Last_Maint} is used by : {a.Users_ID} - {a.Full_Name} - {a.Email}");
            }

            return returnable;
        }

        /// <inheritdoc/>
        public void AddNewDevice(Devices newDevice)
        {
            this.db.Devices.Add(newDevice);
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void Delete(int id)
        {
            var toBeDeleted = this.GetAll().Single(x => x.Device_ID == id);
            this.db.Devices.Remove(toBeDeleted);
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public IQueryable<Devices> GetAll()
        {
            return this.db.Devices;
        }

        /// <inheritdoc/>
        public Devices GetDeviceById(int id)
        {
            return this.GetAll().Single(x => x.Device_ID == id);
        }

        /// <inheritdoc/>
        public void UpdateDevice(Devices device)
        {
            this.db.Devices.Remove(this.GetDeviceById((int)device.Device_ID));
            this.db.SaveChanges();
            this.db.Devices.Add(device);
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public List<string> CountDevicesUsed()
        {
            var q = from d in this.db.Devices
                    join dl in this.db.DeviceLent on d.Device_ID equals dl.Device_ID
                    join u in this.db.colleagues on dl.Users_ID equals u.Users_ID
                    group d by d.Modell into grouped
                    select new
                    {
                        modell = grouped.Key,
                        Count = grouped.Count(),
                    };
            List<string> returnable = new List<string>();
            foreach (var a in q)
            {
                returnable.Add($"{a.modell},{a.Count}");
            }

            return returnable;
        }

        /// <inheritdoc/>
        public List<string> CountDeviceOnStore()
        {
            var q = from d in this.db.Devices
                    orderby d.Modell
                    where d.State_now == "Raktáron"
                    group d by d.Modell into grouped
                    select new
                    {
                        modell = grouped.Key,
                        Count = grouped.Count(),
                    };
            List<string> returnable = new List<string>();
            foreach (var a in q)
            {
                returnable.Add($"{a.modell},{a.Count}");
            }

            return returnable;
        }
    }
}
