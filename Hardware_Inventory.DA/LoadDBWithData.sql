﻿drop table devicelent;
drop table colleagues;
drop table devices;
drop table modell;


CREATE TABLE Modell(
	Modell_ID varchar(255) PRIMARY KEY,
	DeviceType varchar(255),
	CPU varchar(255),
	RAM int,
	Storage_type varchar(255),
	Storage_Size int
);

CREATE TABLE Devices(
	Device_ID int IDENTITY(100000,1) PRIMARY KEY,
	Modell varchar(255) not null,
	Last_Maint date,
	State_now varchar(255),	
	Comments varchar(255),
	FOREIGN KEY(Modell) REFERENCES Modell (Modell_ID)
);


CREATE TABLE colleagues(
	Users_ID int IDENTITY(654321,1) PRIMARY KEY,
	Full_Name varchar(255),
	Email varchar(255),
	WorksAt varchar(255),
	Post varchar(255),
);



CREATE TABLE DeviceLent(
	Users_ID int not null,
	Device_ID int not null,
	Lent_on date default(getdate()),
	FOREIGN KEY (Users_ID) REFERENCES colleagues (Users_ID),
	FOREIGN KEY (Device_ID) REFERENCES Devices (Device_ID)
);

INSERT INTO Modell (Modell_ID,DeviceType,CPU,RAM,Storage_type,Storage_size) Values('Macbook 11 col','Laptop','Intel i5-5250U',4,'SSD',256);
INSERT INTO Modell (Modell_ID,DeviceType,CPU,RAM,Storage_type,Storage_size) Values('Macbook 13 col','Laptop','Intel i7-7540U',8,'SSD',512);
INSERT INTO Modell (Modell_ID,DeviceType,CPU,RAM,Storage_type,Storage_size) Values('Latitude 5401','Laptop','Intel i7-9850H',8,'SSD',512);
INSERT INTO Modell (Modell_ID,DeviceType,CPU,RAM,Storage_type,Storage_size) Values('S9+','Telephone','Snapdragon 845',6,'Flash',64);
INSERT INTO Modell (Modell_ID,DeviceType,CPU,RAM,Storage_type,Storage_size) Values('S9','Telephone','Snapdragon 845',4,'Flash',64);
INSERT INTO Modell (Modell_ID,DeviceType,CPU,RAM,Storage_type,Storage_size) Values('S10','Telephone','Snapdragon 855',8,'Flash',254);
INSERT INTO Modell (Modell_ID,DeviceType,CPU,RAM,Storage_type,Storage_size) Values('S10+','Telephone','Snapdragon 855',8,'Flash',254);
INSERT INTO Modell (Modell_ID,DeviceType,CPU,RAM,Storage_type,Storage_size) Values('iPhone 11','Telephone', 'A13',4,'Flash',128);
INSERT INTO Modell (Modell_ID,DeviceType,CPU,RAM,Storage_type,Storage_size) Values('ThinkPad t480','Laptop', 'intel i5-8250u',8,'SSD',256);
INSERT INTO Modell (Modell_ID,DeviceType,CPU,RAM,Storage_type,Storage_size) Values('Optiplex 3060','Desktop', 'intel i7-9750K',16,'SSD',1028);
INSERT INTO Modell (Modell_ID,DeviceType,CPU,RAM,Storage_type,Storage_size) Values('LENOVO V530','Desktop', 'intel i5-8400',4,'HDD',2054);
INSERT INTO Modell (Modell_ID,DeviceType,CPU,RAM,Storage_type,Storage_size) Values('iMac 27 col','Desktop', 'intel i5-8400',8,'Fusion',1024);



INSERT INTO COLLEAGUES (Full_Name,WorksAt,Post,Email) Values('Trap Pista','Központ','Marketing vezető','trapp@example.com');
INSERT INTO COLLEAGUES (Full_Name,WorksAt,Post,Email) Values('Kun Béla','Központ','Belső ellenőr','kunb@example.com');
INSERT INTO COLLEAGUES (Full_Name,WorksAt,Post,Email) Values('Lapos Elemér','Óbudai Kirendeltség','Fejlesztési vezető','elemer@example.com');
INSERT INTO COLLEAGUES (Full_Name,WorksAt,Post,Email) Values('Példa Béla','Gyöngyösi kirendeltség','Agrár vezető mérnök','peldab@example.com');
INSERT INTO COLLEAGUES (Full_Name,WorksAt,Post,Email) Values('Sárga Villám','Peking','Távolkeleti konzulens','winniethepooh@example.com');
INSERT INTO COLLEAGUES (Full_Name,WorksAt,Post,Email) Values('Béna Béla','Gyöngyösi kirendeltség','Napszámos','benab@example.com');
INSERT INTO COLLEAGUES (Full_Name,WorksAt,Post,Email) Values('Cserepes Virág','Központ','Aszisztens','cserepesv@example.com');
INSERT INTO COLLEAGUES (Full_Name,WorksAt,Post,Email) Values('Kasza Blanka','Központ','Aszisztens','kaszab@example.com');
INSERT INTO COLLEAGUES (Full_Name,WorksAt,Post,Email) Values('Kazi Bácsi','Központ','Portás','kazab@example.com');
INSERT INTO COLLEAGUES (Full_Name,WorksAt,Post,Email) Values('Édes Edina','Központ','Befektetési tanácsadó','edese@example.com');
INSERT INTO COLLEAGUES (Full_Name,WorksAt,Post,Email) Values('Géczi Magdolni','Központ','Területi vezető','geczim@example.com');
INSERT INTO COLLEAGUES (Full_Name,WorksAt,Post,Email) Values('Keresztes Ferenc','Központ','HR vezető','keresztesf@example.com');
INSERT INTO COLLEAGUES (Full_Name,WorksAt,Post,Email) Values('Nagy Dávid','Központ','Munkaügyi Előadó','nagyd@example.com');
INSERT INTO COLLEAGUES (Full_Name,WorksAt,Post,Email) Values('Kiss Ádám','Központ','Operatív vezető','kissa@example.com');
INSERT INTO COLLEAGUES (Full_Name,WorksAt,Post,Email) Values('Lakatos Nintendó','Gyöngyösi kirendeltség','Kúltúrális felelős','lakatn@example.com');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('Macbook 11 col','2019.09.11', 'Felhasználónál');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('Macbook 11 col','2019.09.12', 'Felhasználónál');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('Macbook 11 col','2019.07.11', 'Rossz');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('Macbook 11 col','2019.08.11', 'Rossz');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('Macbook 11 col','2019.09.14', 'Rossz');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('Macbook 11 col','2019.09.14', 'Raktáron');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('Macbook 11 col','2019.09.9', 'Raktáron');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('Macbook 11 col','2019.09.4', 'Raktáron');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('Macbook 11 col','2019.09.5', 'Raktáron');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('Macbook 11 col','2019.09.15', 'Raktáron');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('Macbook 11 col','2019.09.15', 'Raktáron');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('Macbook 11 col','2019.09.15', 'Raktáron');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('Macbook 11 col','2019.09.15', 'Raktáron');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('Macbook 11 col','2019.09.15', 'Szervízben');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('Macbook 13 col','2019.09.10', 'Raktáron');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('Macbook 13 col','2019.09.20', 'Felhasználónál');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('Macbook 13 col','2019.09.12', 'Raktáron');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('Macbook 13 col','2019.07.11', 'Raktáron');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('Macbook 13 col','2018.09.10', 'Raktáron');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('Macbook 13 col','2019.04.10', 'Raktáron');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('Latitude 5401','2019.04.10', 'Raktáron');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('Latitude 5401','2019.04.10', 'Raktáron');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('Latitude 5401','2019.09.14', 'Raktáron');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('Latitude 5401','2019.09.12', 'Raktáron');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('Latitude 5401','2019.09.10', 'Raktáron');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('Latitude 5401','2019.07.10', 'Szervízben');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('Latitude 5401','2019.06.10', 'Szervízben');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('Latitude 5401','2019.05.10', 'Szervízben');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('S9+','2019.09.11', 'Szervízben');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('S9+','2019.05.21', 'Felhasználónál');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('S9','2019.05.12', 'Felhasználónál');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('S9+','2019.05.15', 'Felhasználónál');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('S9+','2019.04.10', 'Raktáron');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('S9','2019.06.19', 'Raktáron');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('S9','2019.05.21', 'Raktáron');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('S10','2018.09.11', 'Raktáron');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('S10','2019.09.11', 'Szervízben');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('S10+','2019.05.21', 'Felhasználónál');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('S10+','2019.05.12', 'Felhasználónál');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('S10','2019.05.15', 'Felhasználónál');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('S10','2019.04.10', 'Raktáron');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('S10','2019.06.19', 'Raktáron');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('S10','2019.05.21', 'Raktáron');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('iPhone 11','2019.06.15', 'Felhasználónál');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('iPhone 11','2019.06.16', 'Felhasználónál');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('iPhone 11','2019.09.11', 'Szervízben');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('iPhone 11','2019.09.12', 'Szervízben');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('iPhone 11','2019.09.13', 'Szervízben');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('iPhone 11','2019.09.14', 'Szervízben');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('iPhone 11','2019.09.15', 'Szervízben');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('iPhone 11','2019.09.16', 'Szervízben');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('iPhone 11','2019.09.17', 'Szervízben');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('iPhone 11','2019.09.18', 'Szervízben');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('ThinkPad t480','2019.09.19', 'Szervízben');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('ThinkPad t480','2019.08.01', 'Felhasználónál');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('ThinkPad t480','2019.08.01', 'Felhasználónál');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('Optiplex 3060','2019.08.11', 'Felhasználónál');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('Optiplex 3060','2019.08.11', 'Felhasználónál');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('Optiplex 3060','2019.08.11', 'Raktáron');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('LENOVO V530','2019.08.11', 'Felhasználónál');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('LENOVO V530','2017.08.11', 'Raktáron');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('LENOVO V530','2018.06.11', 'Raktáron');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('LENOVO V530','2018.06.11', 'Raktáron');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('iMac 27 col','2019.07.11', 'Felhasználónál');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('iMac 27 col','2019.09.12', 'Szervízben');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('iMac 27 col','2019.09.11', 'Szervízben');
INSERT INTO Devices (Modell,Last_Maint,State_now) Values('iMac 27 col','2019.05.11', 'Raktáron');

INSERT INTO DEVICELENT (DEVICE_ID,USERS_ID) VALUES(100000,654321);
INSERT INTO DEVICELENT (DEVICE_ID,USERS_ID) VALUES(100001,654322);
INSERT INTO DEVICELENT (DEVICE_ID,USERS_ID) VALUES(100015,654323);
INSERT INTO DEVICELENT (DEVICE_ID,USERS_ID) VALUES(100029,654333);
INSERT INTO DEVICELENT (DEVICE_ID,USERS_ID) VALUES(100030,654335);
INSERT INTO DEVICELENT (DEVICE_ID,USERS_ID) VALUES(100031,654332);
INSERT INTO DEVICELENT (DEVICE_ID,USERS_ID) VALUES(100037,654335);
INSERT INTO DEVICELENT (DEVICE_ID,USERS_ID) VALUES(100038,654330);
INSERT INTO DEVICELENT (DEVICE_ID,USERS_ID) VALUES(100039,654324);
INSERT INTO DEVICELENT (DEVICE_ID,USERS_ID) VALUES(100043,654325);
INSERT INTO DEVICELENT (DEVICE_ID,USERS_ID) VALUES(100044,654321);
INSERT INTO DEVICELENT (DEVICE_ID,USERS_ID) VALUES(100054,654324);
INSERT INTO DEVICELENT (DEVICE_ID,USERS_ID) VALUES(100055,654331);
INSERT INTO DEVICELENT (DEVICE_ID,USERS_ID) VALUES(100056,654328);
INSERT INTO DEVICELENT (DEVICE_ID,USERS_ID) VALUES(100057,654327);
INSERT INTO DEVICELENT (DEVICE_ID,USERS_ID) VALUES(100059,654334);
INSERT INTO DEVICELENT (DEVICE_ID,USERS_ID) VALUES(100063,654325);
