
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace Hardware_Inventory.DA
{

using System;
    using System.Collections.Generic;
    
public partial class Modell
{

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
    public Modell()
    {

        this.Devices = new HashSet<Devices>();

    }


    public string Modell_ID { get; set; }

    public string DeviceType { get; set; }

    public string CPU { get; set; }

    public Nullable<int> RAM { get; set; }

    public string Storage_type { get; set; }

    public Nullable<int> Storage_Size { get; set; }



    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]

    public virtual ICollection<Devices> Devices { get; set; }

}

}
