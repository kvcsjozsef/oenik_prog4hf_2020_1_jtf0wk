﻿using GalaSoft.MvvmLight.Messaging;
using System;
using System.Windows;

namespace Hardware_Inventory.WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Messenger.Default.Register<string>(this, "ColleagueResult",
                msg =>
                {
                    (DataContext as MainVM).LoadCmd.Execute(null);
                    MessageBox.Show(msg);
                });

            (DataContext as MainVM).EditorFunc = (colleague) =>
            {
                EditorWindow win = new EditorWindow();
                win.DataContext = colleague;
                return (win.ShowDialog() == true);
            };
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Messenger.Default.Unregister(this);
        }
    }
}
