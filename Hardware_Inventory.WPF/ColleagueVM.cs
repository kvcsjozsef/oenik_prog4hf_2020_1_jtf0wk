﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hardware_Inventory.WPF
{
    class ColleagueVM : ObservableObject
    {
		private int id;
		private string name;
		private string mail;
		private string workplace;
		private string post;

		public string Post
		{
			get { return post; }
			set { Set(ref post, value); }
		}


		public string Workplace
		{
			get { return workplace; }
			set { Set(ref workplace, value); }
		}


		public string Mail
		{
			get { return mail; }
			set { Set(ref mail, value); }
		}


		public string Name
		{
			get { return name; }
			set { Set(ref name, value); }
		}


		public int ID
		{
			get { return id; }
			set { Set(ref id, value); }
		}

		public void CopyFrom(ColleagueVM other)
		{
			if (other == null) return;

			this.id = other.id;
			this.name = other.name;
			this.mail = other.mail;
			this.workplace = other.workplace;
			this.post = other.post;
		}

	}
}
