﻿using GalaSoft.MvvmLight.Messaging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Windows.Documents;

namespace Hardware_Inventory.WPF
{
    class MainLogic
    {
        string url = "http://localhost:51284/api/ColleaguesApi/";
        HttpClient httpClient = new HttpClient();

        void SendMessage(bool success)
        {
            string msg = success ? "Operation Completed successfully" : "Operation failed";
            Messenger.Default.Send(msg, "ColleagueResult");
        }

        public List<ColleagueVM> ApiGetColleagues()
        {
            string json = httpClient.GetStringAsync(url + "all").Result;
            var list = JsonConvert.DeserializeObject<List<ColleagueVM>>(json);
            return list;
        }

        public void ApiDelColleague(ColleagueVM colleague)
        {
            bool success = false;
            if (colleague != null)
            {
                string json = httpClient.GetStringAsync(url + "del/" + colleague.ID).Result;
                JObject obj = JObject.Parse(json);
                success = (bool)obj["OperationResult"];
            }
            SendMessage(success);
        }

        bool ApiEditColleague(ColleagueVM colleague, bool isEdited)
        {
            if (colleague == null) return false;

            string myUrl = isEdited ? url + "modify" : url + "add";

            Dictionary<string, string> postData = new Dictionary<string, string>();
            if (isEdited) postData.Add(nameof(ColleagueVM.ID), colleague.ID.ToString());
            postData.Add(nameof(ColleagueVM.Name),colleague.Name);
            postData.Add(nameof(ColleagueVM.Mail), colleague.Mail);
            postData.Add(nameof(ColleagueVM.Post), colleague.Post);
            postData.Add(nameof(ColleagueVM.Workplace), colleague.Workplace);

            string json = httpClient.PostAsync(myUrl, new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;

            JObject jObject = JObject.Parse(json);
            return (bool)jObject["OperationResult"];
        }

        public void EditColleague(ColleagueVM colleague, Func<ColleagueVM,bool> editor)
        {
            ColleagueVM clone = new ColleagueVM();
            if (colleague != null) clone.CopyFrom(colleague);
            bool? success = editor?.Invoke(clone);
            if (success == true)
            {
                if (colleague != null) success = ApiEditColleague(clone, true);
                else success = ApiEditColleague(clone, false);
            }
            SendMessage(success == true);
        }
    }
}
