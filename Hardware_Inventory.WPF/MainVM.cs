﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Hardware_Inventory.WPF
{
    class MainVM : ViewModelBase
    {
        private MainLogic logic;
		private ColleagueVM selectedColleague;
		private ObservableCollection<ColleagueVM> allColleagues;

		public ObservableCollection<ColleagueVM> AllColleagues
		{
			get { return allColleagues; }
			set { Set(ref allColleagues, value); }
		}


		public ColleagueVM SelectedColleague
		{
			get { return selectedColleague; }
			set { Set(ref selectedColleague, value); }
		}

		public ICommand AddCmd { get; private set; }
		public ICommand DelCmd { get; private set; }
		public ICommand ModCmd { get; private set; }
		public ICommand LoadCmd { get; private set; }

		public Func<ColleagueVM, bool> EditorFunc { get; set; }

		public MainVM()
		{
			logic = new MainLogic();

			DelCmd = new RelayCommand(() => logic.ApiDelColleague(selectedColleague));
			AddCmd = new RelayCommand(() => logic.EditColleague(null, EditorFunc));
			ModCmd = new RelayCommand(() => logic.EditColleague(selectedColleague, EditorFunc));
			LoadCmd = new RelayCommand(() => 
			AllColleagues = new ObservableCollection<ColleagueVM>(logic.ApiGetColleagues()));
			
		}


	}
}
