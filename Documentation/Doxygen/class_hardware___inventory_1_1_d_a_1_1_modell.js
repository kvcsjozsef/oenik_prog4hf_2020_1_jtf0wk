var class_hardware___inventory_1_1_d_a_1_1_modell =
[
    [ "Modell", "class_hardware___inventory_1_1_d_a_1_1_modell.html#a837c03a47d2620ce56aed42f5d686b9c", null ],
    [ "CPU", "class_hardware___inventory_1_1_d_a_1_1_modell.html#a66f85a961476423d5ac0045f13fbc97f", null ],
    [ "Devices", "class_hardware___inventory_1_1_d_a_1_1_modell.html#a6d4ff83f06645e5991c6c091f0bcdeef", null ],
    [ "DeviceType", "class_hardware___inventory_1_1_d_a_1_1_modell.html#a4d437b97ffba08a1fdef9f3073820053", null ],
    [ "Modell_ID", "class_hardware___inventory_1_1_d_a_1_1_modell.html#a2f9ed222db893008e0aed99f0ff92a33", null ],
    [ "RAM", "class_hardware___inventory_1_1_d_a_1_1_modell.html#a40d29f90250b4d54034298f890fb9d42", null ],
    [ "Storage_Size", "class_hardware___inventory_1_1_d_a_1_1_modell.html#aac4601777f6bef8389447da0fa2cb11a", null ],
    [ "Storage_type", "class_hardware___inventory_1_1_d_a_1_1_modell.html#ae069243a8b56e2da7ce52785e7531420", null ]
];