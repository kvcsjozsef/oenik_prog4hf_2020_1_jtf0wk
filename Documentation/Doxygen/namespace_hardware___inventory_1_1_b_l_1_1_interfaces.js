var namespace_hardware___inventory_1_1_b_l_1_1_interfaces =
[
    [ "IColleagueService", "interface_hardware___inventory_1_1_b_l_1_1_interfaces_1_1_i_colleague_service.html", "interface_hardware___inventory_1_1_b_l_1_1_interfaces_1_1_i_colleague_service" ],
    [ "IDeviceLentService", "interface_hardware___inventory_1_1_b_l_1_1_interfaces_1_1_i_device_lent_service.html", "interface_hardware___inventory_1_1_b_l_1_1_interfaces_1_1_i_device_lent_service" ],
    [ "IDeviceService", "interface_hardware___inventory_1_1_b_l_1_1_interfaces_1_1_i_device_service.html", "interface_hardware___inventory_1_1_b_l_1_1_interfaces_1_1_i_device_service" ],
    [ "IModellService", "interface_hardware___inventory_1_1_b_l_1_1_interfaces_1_1_i_modell_service.html", "interface_hardware___inventory_1_1_b_l_1_1_interfaces_1_1_i_modell_service" ]
];