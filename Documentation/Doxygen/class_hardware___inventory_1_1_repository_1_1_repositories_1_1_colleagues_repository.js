var class_hardware___inventory_1_1_repository_1_1_repositories_1_1_colleagues_repository =
[
    [ "AddNewColleague", "class_hardware___inventory_1_1_repository_1_1_repositories_1_1_colleagues_repository.html#a0b05f9a8b4e3ba89af4292554266aa38", null ],
    [ "Delete", "class_hardware___inventory_1_1_repository_1_1_repositories_1_1_colleagues_repository.html#a0653a7d5b024907024c5df9c95c129e4", null ],
    [ "GetAll", "class_hardware___inventory_1_1_repository_1_1_repositories_1_1_colleagues_repository.html#a470c62626576418056b1b81a34f465b1", null ],
    [ "GetColleagueById", "class_hardware___inventory_1_1_repository_1_1_repositories_1_1_colleagues_repository.html#a30af9eb2be772c8442f9869820df3bfa", null ],
    [ "UpdateColleague", "class_hardware___inventory_1_1_repository_1_1_repositories_1_1_colleagues_repository.html#a5806e8841a85778bb40cf5463d83a2f2", null ]
];