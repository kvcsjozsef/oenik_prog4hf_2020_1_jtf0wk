var namespace_wpf_application2 =
[
    [ "App", "class_wpf_application2_1_1_app.html", "class_wpf_application2_1_1_app" ],
    [ "MainWindow", "class_wpf_application2_1_1_main_window.html", "class_wpf_application2_1_1_main_window" ],
    [ "MyShape", "class_wpf_application2_1_1_my_shape.html", "class_wpf_application2_1_1_my_shape" ],
    [ "PongControl", "class_wpf_application2_1_1_pong_control.html", "class_wpf_application2_1_1_pong_control" ],
    [ "PongLogic", "class_wpf_application2_1_1_pong_logic.html", "class_wpf_application2_1_1_pong_logic" ],
    [ "PongModel", "class_wpf_application2_1_1_pong_model.html", "class_wpf_application2_1_1_pong_model" ],
    [ "PongRenderer", "class_wpf_application2_1_1_pong_renderer.html", "class_wpf_application2_1_1_pong_renderer" ],
    [ "Star", "class_wpf_application2_1_1_star.html", "class_wpf_application2_1_1_star" ]
];