var class_hardware___inventory_1_1_b_l_1_1_services_1_1_colleague_service =
[
    [ "ColleagueService", "class_hardware___inventory_1_1_b_l_1_1_services_1_1_colleague_service.html#ac5332fc4f68996df37767966a22fc11d", null ],
    [ "ColleagueService", "class_hardware___inventory_1_1_b_l_1_1_services_1_1_colleague_service.html#a0d91f519ec2f781db95c252d3480e04f", null ],
    [ "AddNewColleague", "class_hardware___inventory_1_1_b_l_1_1_services_1_1_colleague_service.html#a349fc365aa12383ef58f6ae1a422d6e7", null ],
    [ "Delete", "class_hardware___inventory_1_1_b_l_1_1_services_1_1_colleague_service.html#ae529c18337c04264ff4ee249365a6b2c", null ],
    [ "GetAll", "class_hardware___inventory_1_1_b_l_1_1_services_1_1_colleague_service.html#abe66533958315be8e9214a8c2b3220b3", null ],
    [ "GetColleagueByID", "class_hardware___inventory_1_1_b_l_1_1_services_1_1_colleague_service.html#a939da7a2cf74576f1fa960ee3a1cbc33", null ],
    [ "UpdateColleague", "class_hardware___inventory_1_1_b_l_1_1_services_1_1_colleague_service.html#a2f61ab4293ad2431dbbf537630298b83", null ],
    [ "ColleagueRepository", "class_hardware___inventory_1_1_b_l_1_1_services_1_1_colleague_service.html#adf3ea5a94ffe70c3eb7e270f51d4a3a3", null ]
];