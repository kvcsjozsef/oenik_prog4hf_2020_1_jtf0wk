var class_hardware___inventory_1_1_repository_1_1_repositories_1_1_modell_repository =
[
    [ "AddNewModell", "class_hardware___inventory_1_1_repository_1_1_repositories_1_1_modell_repository.html#aef87cc17a70a9a5715a33591a1f63336", null ],
    [ "Delete", "class_hardware___inventory_1_1_repository_1_1_repositories_1_1_modell_repository.html#afc6afa11a4067fd9c0f2e1b98954f8e6", null ],
    [ "GetAll", "class_hardware___inventory_1_1_repository_1_1_repositories_1_1_modell_repository.html#ac95e70a550fb5a8e15dbcd1110e63460", null ],
    [ "GetModellById", "class_hardware___inventory_1_1_repository_1_1_repositories_1_1_modell_repository.html#a5e2f351f1fa19c4f4ed75d2b6c17870a", null ],
    [ "UpdateModell", "class_hardware___inventory_1_1_repository_1_1_repositories_1_1_modell_repository.html#a555f9b48bfffe7f40617165044da59af", null ]
];