var interface_hardware___inventory_1_1_b_l_1_1_interfaces_1_1_i_device_lent_service =
[
    [ "AddNewDeviceLent", "interface_hardware___inventory_1_1_b_l_1_1_interfaces_1_1_i_device_lent_service.html#a21174891eddc86b098836b3328b588c7", null ],
    [ "Delete", "interface_hardware___inventory_1_1_b_l_1_1_interfaces_1_1_i_device_lent_service.html#a7998baf43186a25d6143531be9bfe206", null ],
    [ "GetAll", "interface_hardware___inventory_1_1_b_l_1_1_interfaces_1_1_i_device_lent_service.html#a6f262b604a5e52ba21f728121915d45a", null ],
    [ "GetDeviceLentByDeviceID", "interface_hardware___inventory_1_1_b_l_1_1_interfaces_1_1_i_device_lent_service.html#ab1b26721e99654fd11284b8a159c657d", null ],
    [ "GetDeviceLentByUserID", "interface_hardware___inventory_1_1_b_l_1_1_interfaces_1_1_i_device_lent_service.html#a74ba191619af78dc7b4177f982a902af", null ],
    [ "UpdateDeviceLent", "interface_hardware___inventory_1_1_b_l_1_1_interfaces_1_1_i_device_lent_service.html#a0e86db5611040bd84e4ee70c185ed0c3", null ]
];