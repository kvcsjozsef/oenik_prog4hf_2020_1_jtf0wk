var interface_hardware___inventory_1_1_repository_1_1_interfaces_1_1_i_device_lent_repository =
[
    [ "AddNewDeviceLent", "interface_hardware___inventory_1_1_repository_1_1_interfaces_1_1_i_device_lent_repository.html#a1016557b2922ab31f36bfec5ebac25f9", null ],
    [ "Delete", "interface_hardware___inventory_1_1_repository_1_1_interfaces_1_1_i_device_lent_repository.html#ad7d919583ea2d73ebfb0ae6344d362e1", null ],
    [ "GetAll", "interface_hardware___inventory_1_1_repository_1_1_interfaces_1_1_i_device_lent_repository.html#aa8aca5511f2ba430aaecf0caeb571119", null ],
    [ "GetDeviceLentByDeviceId", "interface_hardware___inventory_1_1_repository_1_1_interfaces_1_1_i_device_lent_repository.html#a840a6cfe65aeaa8639572cf86c7d873b", null ],
    [ "GetDeviceLentByUserId", "interface_hardware___inventory_1_1_repository_1_1_interfaces_1_1_i_device_lent_repository.html#ab520e098452af45ba984d514a42a20a4", null ],
    [ "UpdateDeviceLent", "interface_hardware___inventory_1_1_repository_1_1_interfaces_1_1_i_device_lent_repository.html#ac71d49687e011dbfdb87b25162515dd0", null ]
];