var class_hardware___inventory_1_1_b_l_1_1_services_1_1_device_service =
[
    [ "DeviceService", "class_hardware___inventory_1_1_b_l_1_1_services_1_1_device_service.html#a0e13cab7339513ea931c78035f9c2a85", null ],
    [ "DeviceService", "class_hardware___inventory_1_1_b_l_1_1_services_1_1_device_service.html#a2f84f9c6604c213ed96595055f798c40", null ],
    [ "AddNewDevice", "class_hardware___inventory_1_1_b_l_1_1_services_1_1_device_service.html#accad37450dfc70f9437afbe973f59c40", null ],
    [ "AllDevicesWithUsers", "class_hardware___inventory_1_1_b_l_1_1_services_1_1_device_service.html#adb384c36f7617033b31fe065d939f5f5", null ],
    [ "CountDeviceOnStore", "class_hardware___inventory_1_1_b_l_1_1_services_1_1_device_service.html#ab34902d5a4f667f643cb0d2ada1cb1df", null ],
    [ "CountDevicesUsed", "class_hardware___inventory_1_1_b_l_1_1_services_1_1_device_service.html#a3de0b9ab52f0718b4e0355d9a5848a39", null ],
    [ "Delete", "class_hardware___inventory_1_1_b_l_1_1_services_1_1_device_service.html#a7c292b65c4534565b66026e324004d44", null ],
    [ "GetAll", "class_hardware___inventory_1_1_b_l_1_1_services_1_1_device_service.html#a05d7c5defe07ce6731a610f4b7af08e9", null ],
    [ "GetDeviceByID", "class_hardware___inventory_1_1_b_l_1_1_services_1_1_device_service.html#afcd8f104ecdbe022a881f27c54654c9e", null ],
    [ "UpdateDevice", "class_hardware___inventory_1_1_b_l_1_1_services_1_1_device_service.html#ad88ff830a2151c274bc07a9ee5126c3a", null ],
    [ "DeviceRepository", "class_hardware___inventory_1_1_b_l_1_1_services_1_1_device_service.html#a3b247f396e6b4f74f9ac1fb265a771e8", null ]
];