var interface_hardware___inventory_1_1_b_l_1_1_interfaces_1_1_i_modell_service =
[
    [ "AddNewModell", "interface_hardware___inventory_1_1_b_l_1_1_interfaces_1_1_i_modell_service.html#ad35eb9f89d951ff59e97403865ba112b", null ],
    [ "Delete", "interface_hardware___inventory_1_1_b_l_1_1_interfaces_1_1_i_modell_service.html#a1d0fb69f48c96c69561238d9a583e255", null ],
    [ "GetAll", "interface_hardware___inventory_1_1_b_l_1_1_interfaces_1_1_i_modell_service.html#a29a5da4afc134c7349419ef5928bd96d", null ],
    [ "GetModellByID", "interface_hardware___inventory_1_1_b_l_1_1_interfaces_1_1_i_modell_service.html#a172827a54eab11a7f168433dddee677b", null ],
    [ "UpdateModell", "interface_hardware___inventory_1_1_b_l_1_1_interfaces_1_1_i_modell_service.html#a89d495dea781a07057c2792944d00d1e", null ]
];