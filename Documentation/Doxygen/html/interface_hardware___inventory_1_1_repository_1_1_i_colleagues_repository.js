var interface_hardware___inventory_1_1_repository_1_1_i_colleagues_repository =
[
    [ "AddNewColleague", "interface_hardware___inventory_1_1_repository_1_1_i_colleagues_repository.html#a10f3ba213c856c527daf591ac23276c1", null ],
    [ "Delete", "interface_hardware___inventory_1_1_repository_1_1_i_colleagues_repository.html#afb3ec49a5a612ae76dd8a2208fb52071", null ],
    [ "GetAll", "interface_hardware___inventory_1_1_repository_1_1_i_colleagues_repository.html#a375596957c83443ff510993eb6bbb954", null ],
    [ "GetColleagueById", "interface_hardware___inventory_1_1_repository_1_1_i_colleagues_repository.html#ae162c2d83d9f5b005a331d47bfb59755", null ],
    [ "UpdateColleague", "interface_hardware___inventory_1_1_repository_1_1_i_colleagues_repository.html#ae173aeaed4f8bc9272b4842e4d6d866e", null ]
];