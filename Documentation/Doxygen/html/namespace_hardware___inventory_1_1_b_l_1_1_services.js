var namespace_hardware___inventory_1_1_b_l_1_1_services =
[
    [ "ColleagueService", "class_hardware___inventory_1_1_b_l_1_1_services_1_1_colleague_service.html", "class_hardware___inventory_1_1_b_l_1_1_services_1_1_colleague_service" ],
    [ "DeviceLentService", "class_hardware___inventory_1_1_b_l_1_1_services_1_1_device_lent_service.html", "class_hardware___inventory_1_1_b_l_1_1_services_1_1_device_lent_service" ],
    [ "DeviceService", "class_hardware___inventory_1_1_b_l_1_1_services_1_1_device_service.html", "class_hardware___inventory_1_1_b_l_1_1_services_1_1_device_service" ],
    [ "ModellService", "class_hardware___inventory_1_1_b_l_1_1_services_1_1_modell_service.html", "class_hardware___inventory_1_1_b_l_1_1_services_1_1_modell_service" ]
];