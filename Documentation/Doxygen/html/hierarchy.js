var hierarchy =
[
    [ "ApiController", null, [
      [ "Hardware_Inventory.Web.Controllers.ColleaguesApiController", "class_hardware___inventory_1_1_web_1_1_controllers_1_1_colleagues_api_controller.html", null ]
    ] ],
    [ "Hardware_Inventory.Web.Controllers.ColleaguesApiController.ApiResult", "class_hardware___inventory_1_1_web_1_1_controllers_1_1_colleagues_api_controller_1_1_api_result.html", null ],
    [ "Application", null, [
      [ "Hardware_Inventory.WPF.App", "class_hardware___inventory_1_1_w_p_f_1_1_app.html", null ],
      [ "WpfApplication2.App", "class_wpf_application2_1_1_app.html", null ]
    ] ],
    [ "Hardware_Inventory.ConsoleClient.Colleague", "class_hardware___inventory_1_1_console_client_1_1_colleague.html", null ],
    [ "Hardware_Inventory.Web.Models.Colleague", "class_hardware___inventory_1_1_web_1_1_models_1_1_colleague.html", null ],
    [ "Hardware_Inventory.DA.colleagues", "class_hardware___inventory_1_1_d_a_1_1colleagues.html", null ],
    [ "Hardware_Inventory.Web.Models.ColleaguesViewModel", "class_hardware___inventory_1_1_web_1_1_models_1_1_colleagues_view_model.html", null ],
    [ "Hardware_Inventory.Tests.ColleagueTests", "class_hardware___inventory_1_1_tests_1_1_colleague_tests.html", null ],
    [ "Controller", null, [
      [ "Hardware_Inventory.Web.Views.ColleaguesController", "class_hardware___inventory_1_1_web_1_1_views_1_1_colleagues_controller.html", null ],
      [ "WebApplication1.Controllers.CalorieCounterController", "class_web_application1_1_1_controllers_1_1_calorie_counter_controller.html", null ],
      [ "WebApplication1.Controllers.HomeController", "class_web_application1_1_1_controllers_1_1_home_controller.html", null ]
    ] ],
    [ "DbContext", null, [
      [ "Hardware_Inventory.DA.HardwareInventory", "class_hardware___inventory_1_1_d_a_1_1_hardware_inventory.html", null ]
    ] ],
    [ "Hardware_Inventory.DA.DeviceLent", "class_hardware___inventory_1_1_d_a_1_1_device_lent.html", null ],
    [ "Hardware_Inventory.Tests.DeviceLentTests", "class_hardware___inventory_1_1_tests_1_1_device_lent_tests.html", null ],
    [ "Hardware_Inventory.BL.DeviceModellAndCount", "class_hardware___inventory_1_1_b_l_1_1_device_modell_and_count.html", null ],
    [ "Hardware_Inventory.DA.Devices", "class_hardware___inventory_1_1_d_a_1_1_devices.html", null ],
    [ "Hardware_Inventory.Tests.DeviceTests", "class_hardware___inventory_1_1_tests_1_1_device_tests.html", null ],
    [ "WebApplication1.Models.ExerciseInput", "class_web_application1_1_1_models_1_1_exercise_input.html", [
      [ "WebApplication1.Models.ExerciseOutput", "class_web_application1_1_1_models_1_1_exercise_output.html", null ]
    ] ],
    [ "WebApplication1.Models.Exercises", "class_web_application1_1_1_models_1_1_exercises.html", null ],
    [ "FrameworkElement", null, [
      [ "WpfApplication2.PongControl", "class_wpf_application2_1_1_pong_control.html", null ]
    ] ],
    [ "HttpApplication", null, [
      [ "Hardware_Inventory.Web.Global", "class_hardware___inventory_1_1_web_1_1_global.html", null ]
    ] ],
    [ "Hardware_Inventory.BL.Interfaces.IColleagueService", "interface_hardware___inventory_1_1_b_l_1_1_interfaces_1_1_i_colleague_service.html", [
      [ "Hardware_Inventory.BL.Services.ColleagueService", "class_hardware___inventory_1_1_b_l_1_1_services_1_1_colleague_service.html", null ]
    ] ],
    [ "Hardware_Inventory.Repository.IColleaguesRepository", "interface_hardware___inventory_1_1_repository_1_1_i_colleagues_repository.html", [
      [ "Hardware_Inventory.Repository.Repositories.ColleaguesRepository", "class_hardware___inventory_1_1_repository_1_1_repositories_1_1_colleagues_repository.html", null ]
    ] ],
    [ "Hardware_Inventory.Repository.Interfaces.IDeviceLentRepository", "interface_hardware___inventory_1_1_repository_1_1_interfaces_1_1_i_device_lent_repository.html", [
      [ "Hardware_Inventory.Repository.Repositories.DeviceLentRepository", "class_hardware___inventory_1_1_repository_1_1_repositories_1_1_device_lent_repository.html", null ]
    ] ],
    [ "Hardware_Inventory.BL.Interfaces.IDeviceLentService", "interface_hardware___inventory_1_1_b_l_1_1_interfaces_1_1_i_device_lent_service.html", [
      [ "Hardware_Inventory.BL.Services.DeviceLentService", "class_hardware___inventory_1_1_b_l_1_1_services_1_1_device_lent_service.html", null ]
    ] ],
    [ "Hardware_Inventory.BL.Interfaces.IDeviceService", "interface_hardware___inventory_1_1_b_l_1_1_interfaces_1_1_i_device_service.html", [
      [ "Hardware_Inventory.BL.Services.DeviceService", "class_hardware___inventory_1_1_b_l_1_1_services_1_1_device_service.html", null ]
    ] ],
    [ "Hardware_Inventory.Repository.Interfaces.IDevicesRepository", "interface_hardware___inventory_1_1_repository_1_1_interfaces_1_1_i_devices_repository.html", [
      [ "Hardware_Inventory.Repository.Repositories.DevicesRepository", "class_hardware___inventory_1_1_repository_1_1_repositories_1_1_devices_repository.html", null ]
    ] ],
    [ "Hardware_Inventory.Repository.Interfaces.IModellRepository", "interface_hardware___inventory_1_1_repository_1_1_interfaces_1_1_i_modell_repository.html", [
      [ "Hardware_Inventory.Repository.Repositories.ModellRepository", "class_hardware___inventory_1_1_repository_1_1_repositories_1_1_modell_repository.html", null ]
    ] ],
    [ "Hardware_Inventory.BL.Interfaces.IModellService", "interface_hardware___inventory_1_1_b_l_1_1_interfaces_1_1_i_modell_service.html", [
      [ "Hardware_Inventory.BL.Services.ModellService", "class_hardware___inventory_1_1_b_l_1_1_services_1_1_modell_service.html", null ]
    ] ],
    [ "InternalTypeHelper", null, [
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ]
    ] ],
    [ "Hardware_Inventory.WPF.MainLogic", "class_hardware___inventory_1_1_w_p_f_1_1_main_logic.html", null ],
    [ "Hardware_Inventory.Web.Models.MapperFactory", "class_hardware___inventory_1_1_web_1_1_models_1_1_mapper_factory.html", null ],
    [ "Hardware_Inventory.DA.Modell", "class_hardware___inventory_1_1_d_a_1_1_modell.html", null ],
    [ "Hardware_Inventory.Tests.ModellTests", "class_hardware___inventory_1_1_tests_1_1_modell_tests.html", null ],
    [ "WpfApplication2.MyShape", "class_wpf_application2_1_1_my_shape.html", [
      [ "WpfApplication2.Star", "class_wpf_application2_1_1_star.html", null ]
    ] ],
    [ "Hardware_Inventory.BL.NewDeviceFromWeb", "class_hardware___inventory_1_1_b_l_1_1_new_device_from_web.html", null ],
    [ "Hardware_Inventory.BL.NonCrudOperations", "class_hardware___inventory_1_1_b_l_1_1_non_crud_operations.html", null ],
    [ "ObservableObject", null, [
      [ "Hardware_Inventory.WPF.ColleagueVM", "class_hardware___inventory_1_1_w_p_f_1_1_colleague_v_m.html", null ]
    ] ],
    [ "WpfApplication2.PongLogic", "class_wpf_application2_1_1_pong_logic.html", null ],
    [ "WpfApplication2.PongModel", "class_wpf_application2_1_1_pong_model.html", null ],
    [ "WpfApplication2.PongRenderer", "class_wpf_application2_1_1_pong_renderer.html", null ],
    [ "Hardware_Inventory.ConsoleClient.Program", "class_hardware___inventory_1_1_console_client_1_1_program.html", null ],
    [ "Hardware_Inventory.Program", "class_hardware___inventory_1_1_program.html", null ],
    [ "CarShop.Web.RouteConfig", "class_car_shop_1_1_web_1_1_route_config.html", null ],
    [ "ViewModelBase", null, [
      [ "Hardware_Inventory.WPF.MainVM", "class_hardware___inventory_1_1_w_p_f_1_1_main_v_m.html", null ]
    ] ],
    [ "Window", null, [
      [ "Hardware_Inventory.WPF.EditorWindow", "class_hardware___inventory_1_1_w_p_f_1_1_editor_window.html", null ],
      [ "Hardware_Inventory.WPF.MainWindow", "class_hardware___inventory_1_1_w_p_f_1_1_main_window.html", null ],
      [ "WpfApplication2.MainWindow", "class_wpf_application2_1_1_main_window.html", null ]
    ] ]
];