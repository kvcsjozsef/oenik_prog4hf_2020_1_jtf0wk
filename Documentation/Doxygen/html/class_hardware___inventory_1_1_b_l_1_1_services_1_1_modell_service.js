var class_hardware___inventory_1_1_b_l_1_1_services_1_1_modell_service =
[
    [ "ModellService", "class_hardware___inventory_1_1_b_l_1_1_services_1_1_modell_service.html#a41c8db1b8ef2b043aa2eb0fa06f92ff1", null ],
    [ "ModellService", "class_hardware___inventory_1_1_b_l_1_1_services_1_1_modell_service.html#a0040e8a53c69b281a3825914b122529c", null ],
    [ "AddNewModell", "class_hardware___inventory_1_1_b_l_1_1_services_1_1_modell_service.html#a0b37c34dd8a80464a560c0c85c5a8f5c", null ],
    [ "Delete", "class_hardware___inventory_1_1_b_l_1_1_services_1_1_modell_service.html#a08185bfb1edbca865dcabfaebe1668f1", null ],
    [ "GetAll", "class_hardware___inventory_1_1_b_l_1_1_services_1_1_modell_service.html#ad0b20071c422973da0091dd1e6400050", null ],
    [ "GetModellByID", "class_hardware___inventory_1_1_b_l_1_1_services_1_1_modell_service.html#ae669955e86a2c8689d1110d8bb78e400", null ],
    [ "UpdateModell", "class_hardware___inventory_1_1_b_l_1_1_services_1_1_modell_service.html#ac097e58264ab53ffff899110e254f9f9", null ],
    [ "ModellRepository", "class_hardware___inventory_1_1_b_l_1_1_services_1_1_modell_service.html#a08df24f34b70c7da720cd2a834b2758d", null ]
];