var interface_hardware___inventory_1_1_b_l_1_1_interfaces_1_1_i_colleague_service =
[
    [ "AddNewColleague", "interface_hardware___inventory_1_1_b_l_1_1_interfaces_1_1_i_colleague_service.html#ad6da1ebfc93fff01fad5016850a2724b", null ],
    [ "Delete", "interface_hardware___inventory_1_1_b_l_1_1_interfaces_1_1_i_colleague_service.html#a64167321c07cb46981353883aaab2ef3", null ],
    [ "GetAll", "interface_hardware___inventory_1_1_b_l_1_1_interfaces_1_1_i_colleague_service.html#a058572a950111103dd0f45487b1d1ea8", null ],
    [ "GetColleagueByID", "interface_hardware___inventory_1_1_b_l_1_1_interfaces_1_1_i_colleague_service.html#a95b9c0e3794fbec12adb020e92193c1c", null ],
    [ "UpdateColleague", "interface_hardware___inventory_1_1_b_l_1_1_interfaces_1_1_i_colleague_service.html#adb003f82c438f2967462d64fa5d58678", null ]
];