var class_hardware___inventory_1_1_web_1_1_controllers_1_1_colleagues_api_controller =
[
    [ "ApiResult", "class_hardware___inventory_1_1_web_1_1_controllers_1_1_colleagues_api_controller_1_1_api_result.html", "class_hardware___inventory_1_1_web_1_1_controllers_1_1_colleagues_api_controller_1_1_api_result" ],
    [ "ColleaguesApiController", "class_hardware___inventory_1_1_web_1_1_controllers_1_1_colleagues_api_controller.html#acaa9552b8f7875382f0a6c01f128374f", null ],
    [ "AddOneColleague", "class_hardware___inventory_1_1_web_1_1_controllers_1_1_colleagues_api_controller.html#a822b348194e774bfa6d602f76c27194a", null ],
    [ "DelOneColleague", "class_hardware___inventory_1_1_web_1_1_controllers_1_1_colleagues_api_controller.html#afd20852b41486b5211c5b635af567379", null ],
    [ "GetAll", "class_hardware___inventory_1_1_web_1_1_controllers_1_1_colleagues_api_controller.html#ae737eb3983c350aa7e55c082d7119f5a", null ],
    [ "ModifyOneColleague", "class_hardware___inventory_1_1_web_1_1_controllers_1_1_colleagues_api_controller.html#aad610ef5f5d62e324b3aec89415109db", null ]
];