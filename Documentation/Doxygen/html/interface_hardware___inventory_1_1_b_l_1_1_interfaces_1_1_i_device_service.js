var interface_hardware___inventory_1_1_b_l_1_1_interfaces_1_1_i_device_service =
[
    [ "AddNewDevice", "interface_hardware___inventory_1_1_b_l_1_1_interfaces_1_1_i_device_service.html#a681bb06aa35a16f31c36108063d41017", null ],
    [ "AllDevicesWithUsers", "interface_hardware___inventory_1_1_b_l_1_1_interfaces_1_1_i_device_service.html#af9cd862e6b39bd98a0e3b2fcb9d55311", null ],
    [ "CountDeviceOnStore", "interface_hardware___inventory_1_1_b_l_1_1_interfaces_1_1_i_device_service.html#a386a23adb027c273c1b607c9875b55b2", null ],
    [ "CountDevicesUsed", "interface_hardware___inventory_1_1_b_l_1_1_interfaces_1_1_i_device_service.html#ae529db24d9713fa874cf4b5f0d443ee9", null ],
    [ "Delete", "interface_hardware___inventory_1_1_b_l_1_1_interfaces_1_1_i_device_service.html#aee1d026badc643991adeed510ac9bedf", null ],
    [ "GetAll", "interface_hardware___inventory_1_1_b_l_1_1_interfaces_1_1_i_device_service.html#aa45efdf3510c32baed1470bb652d1727", null ],
    [ "GetDeviceByID", "interface_hardware___inventory_1_1_b_l_1_1_interfaces_1_1_i_device_service.html#a75ab255bd39f2d40ec3254af332cb8d7", null ],
    [ "UpdateDevice", "interface_hardware___inventory_1_1_b_l_1_1_interfaces_1_1_i_device_service.html#aeaa547f2416bf7c43236087cbdd283b9", null ]
];