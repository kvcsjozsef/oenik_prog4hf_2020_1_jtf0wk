var namespace_hardware___inventory =
[
    [ "BL", "namespace_hardware___inventory_1_1_b_l.html", "namespace_hardware___inventory_1_1_b_l" ],
    [ "ConsoleClient", "namespace_hardware___inventory_1_1_console_client.html", "namespace_hardware___inventory_1_1_console_client" ],
    [ "DA", "namespace_hardware___inventory_1_1_d_a.html", "namespace_hardware___inventory_1_1_d_a" ],
    [ "Repository", "namespace_hardware___inventory_1_1_repository.html", "namespace_hardware___inventory_1_1_repository" ],
    [ "Tests", "namespace_hardware___inventory_1_1_tests.html", "namespace_hardware___inventory_1_1_tests" ],
    [ "Web", "namespace_hardware___inventory_1_1_web.html", "namespace_hardware___inventory_1_1_web" ],
    [ "WPF", "namespace_hardware___inventory_1_1_w_p_f.html", "namespace_hardware___inventory_1_1_w_p_f" ],
    [ "Program", "class_hardware___inventory_1_1_program.html", null ]
];