var class_wpf_application2_1_1_pong_logic =
[
    [ "Direction", "class_wpf_application2_1_1_pong_logic.html#a0a41d2546a7ba9dee49b221f6859c8a4", [
      [ "Left", "class_wpf_application2_1_1_pong_logic.html#a0a41d2546a7ba9dee49b221f6859c8a4a945d5e233cf7d6240f6b783b36a374ff", null ],
      [ "Right", "class_wpf_application2_1_1_pong_logic.html#a0a41d2546a7ba9dee49b221f6859c8a4a92b09c7c48c520c3c55e497875da437c", null ]
    ] ],
    [ "PongLogic", "class_wpf_application2_1_1_pong_logic.html#ac0a2915326bf4737f9fb03c099111b95", null ],
    [ "AddStar", "class_wpf_application2_1_1_pong_logic.html#a6ccf42e879e91579f922c52b8cf74bc7", null ],
    [ "JumpPad", "class_wpf_application2_1_1_pong_logic.html#a819080d55a17f1f803366ce5d2b7c6a5", null ],
    [ "MoveBall", "class_wpf_application2_1_1_pong_logic.html#acb9e992356756a429fd405c48eeac141", null ],
    [ "MoveEnemy", "class_wpf_application2_1_1_pong_logic.html#a79c0c3cb6367aedcf0eda88bf0b7103e", null ],
    [ "MoveEnemyBackRound", "class_wpf_application2_1_1_pong_logic.html#a91b215175f3359601d32aca79e859c20", null ],
    [ "MovePad", "class_wpf_application2_1_1_pong_logic.html#afbeaf179f59b750bc6f82b641bb40e19", null ],
    [ "MoveShape", "class_wpf_application2_1_1_pong_logic.html#a14e9d222e7b8d3b3b69f85fa91725629", null ],
    [ "MoveStars", "class_wpf_application2_1_1_pong_logic.html#af99c31183f28339a4147fcea23871c63", null ],
    [ "rnd", "class_wpf_application2_1_1_pong_logic.html#ae1c4a6b434ee051856da1be840b92722", null ],
    [ "RefreshScreen", "class_wpf_application2_1_1_pong_logic.html#af608ace34475209c20ea070267005012", null ]
];