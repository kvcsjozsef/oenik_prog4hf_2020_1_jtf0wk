var namespace_hardware___inventory_1_1_b_l =
[
    [ "Interfaces", "namespace_hardware___inventory_1_1_b_l_1_1_interfaces.html", "namespace_hardware___inventory_1_1_b_l_1_1_interfaces" ],
    [ "Services", "namespace_hardware___inventory_1_1_b_l_1_1_services.html", "namespace_hardware___inventory_1_1_b_l_1_1_services" ],
    [ "DeviceModellAndCount", "class_hardware___inventory_1_1_b_l_1_1_device_modell_and_count.html", "class_hardware___inventory_1_1_b_l_1_1_device_modell_and_count" ],
    [ "NewDeviceFromWeb", "class_hardware___inventory_1_1_b_l_1_1_new_device_from_web.html", "class_hardware___inventory_1_1_b_l_1_1_new_device_from_web" ],
    [ "NonCrudOperations", "class_hardware___inventory_1_1_b_l_1_1_non_crud_operations.html", null ]
];