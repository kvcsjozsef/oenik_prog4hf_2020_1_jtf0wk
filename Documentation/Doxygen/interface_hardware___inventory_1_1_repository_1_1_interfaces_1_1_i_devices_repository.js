var interface_hardware___inventory_1_1_repository_1_1_interfaces_1_1_i_devices_repository =
[
    [ "AddNewDevice", "interface_hardware___inventory_1_1_repository_1_1_interfaces_1_1_i_devices_repository.html#a36b39877ef4b8489f6ab6b48c1592971", null ],
    [ "CountDeviceOnStore", "interface_hardware___inventory_1_1_repository_1_1_interfaces_1_1_i_devices_repository.html#a1ef6cdb9307cb1a218111e74b8cbd25a", null ],
    [ "CountDevicesUsed", "interface_hardware___inventory_1_1_repository_1_1_interfaces_1_1_i_devices_repository.html#aa5a97769f6a1cecd5aa7d8ae56455d28", null ],
    [ "Delete", "interface_hardware___inventory_1_1_repository_1_1_interfaces_1_1_i_devices_repository.html#af512f96d454c038c1791752776212ed7", null ],
    [ "GetAll", "interface_hardware___inventory_1_1_repository_1_1_interfaces_1_1_i_devices_repository.html#ac039463d79f093eb5938846cc735e043", null ],
    [ "GetAllDeviceWithUser", "interface_hardware___inventory_1_1_repository_1_1_interfaces_1_1_i_devices_repository.html#a463a932d5286994802e753083fce1a57", null ],
    [ "GetDeviceById", "interface_hardware___inventory_1_1_repository_1_1_interfaces_1_1_i_devices_repository.html#ab3a4e94a62ac0d6bce9a3f5d862fdacb", null ],
    [ "UpdateDevice", "interface_hardware___inventory_1_1_repository_1_1_interfaces_1_1_i_devices_repository.html#a7379eb8003b07762743618fc193c1736", null ]
];