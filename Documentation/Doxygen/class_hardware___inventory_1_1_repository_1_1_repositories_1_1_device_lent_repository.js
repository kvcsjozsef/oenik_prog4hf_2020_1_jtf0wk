var class_hardware___inventory_1_1_repository_1_1_repositories_1_1_device_lent_repository =
[
    [ "AddNewDeviceLent", "class_hardware___inventory_1_1_repository_1_1_repositories_1_1_device_lent_repository.html#af38098028758330f7da521fd5c8a3bdc", null ],
    [ "Delete", "class_hardware___inventory_1_1_repository_1_1_repositories_1_1_device_lent_repository.html#af08d428e86b81094899d935f2c0a6fbb", null ],
    [ "GetAll", "class_hardware___inventory_1_1_repository_1_1_repositories_1_1_device_lent_repository.html#ae6f128a57ff56633f11d065c113a7fa9", null ],
    [ "GetDeviceLentByDeviceId", "class_hardware___inventory_1_1_repository_1_1_repositories_1_1_device_lent_repository.html#a501f264134bda1f80ff5dfae4a691dd8", null ],
    [ "GetDeviceLentByUserId", "class_hardware___inventory_1_1_repository_1_1_repositories_1_1_device_lent_repository.html#a8a4101a59b9ad15ae1bf5a5fa3ec8338", null ],
    [ "UpdateDeviceLent", "class_hardware___inventory_1_1_repository_1_1_repositories_1_1_device_lent_repository.html#af8a4f5c8e14bcd18f0e4743bf021914b", null ]
];