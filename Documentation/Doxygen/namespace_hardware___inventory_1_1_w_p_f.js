var namespace_hardware___inventory_1_1_w_p_f =
[
    [ "App", "class_hardware___inventory_1_1_w_p_f_1_1_app.html", "class_hardware___inventory_1_1_w_p_f_1_1_app" ],
    [ "ColleagueVM", "class_hardware___inventory_1_1_w_p_f_1_1_colleague_v_m.html", "class_hardware___inventory_1_1_w_p_f_1_1_colleague_v_m" ],
    [ "EditorWindow", "class_hardware___inventory_1_1_w_p_f_1_1_editor_window.html", "class_hardware___inventory_1_1_w_p_f_1_1_editor_window" ],
    [ "MainLogic", "class_hardware___inventory_1_1_w_p_f_1_1_main_logic.html", "class_hardware___inventory_1_1_w_p_f_1_1_main_logic" ],
    [ "MainVM", "class_hardware___inventory_1_1_w_p_f_1_1_main_v_m.html", "class_hardware___inventory_1_1_w_p_f_1_1_main_v_m" ],
    [ "MainWindow", "class_hardware___inventory_1_1_w_p_f_1_1_main_window.html", "class_hardware___inventory_1_1_w_p_f_1_1_main_window" ]
];