var interface_hardware___inventory_1_1_repository_1_1_interfaces_1_1_i_modell_repository =
[
    [ "AddNewModell", "interface_hardware___inventory_1_1_repository_1_1_interfaces_1_1_i_modell_repository.html#aa0076aafa780fb8a8428b19fab672d0a", null ],
    [ "Delete", "interface_hardware___inventory_1_1_repository_1_1_interfaces_1_1_i_modell_repository.html#a5f098950a84341d3d55953bb3d1bfa8f", null ],
    [ "GetAll", "interface_hardware___inventory_1_1_repository_1_1_interfaces_1_1_i_modell_repository.html#aeae49f799c00051865c55586abfe0f9e", null ],
    [ "GetModellById", "interface_hardware___inventory_1_1_repository_1_1_interfaces_1_1_i_modell_repository.html#a5cb940a739a9fcf3eb5ec5c5745d49c8", null ],
    [ "UpdateModell", "interface_hardware___inventory_1_1_repository_1_1_interfaces_1_1_i_modell_repository.html#ad8f63b76367d39196c4e8bd8b4e26976", null ]
];