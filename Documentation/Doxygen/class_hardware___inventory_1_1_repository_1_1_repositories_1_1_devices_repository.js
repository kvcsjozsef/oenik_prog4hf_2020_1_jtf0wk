var class_hardware___inventory_1_1_repository_1_1_repositories_1_1_devices_repository =
[
    [ "AddNewDevice", "class_hardware___inventory_1_1_repository_1_1_repositories_1_1_devices_repository.html#aabf1cb20e97cbf0cbb6cae99ccfd83c6", null ],
    [ "CountDeviceOnStore", "class_hardware___inventory_1_1_repository_1_1_repositories_1_1_devices_repository.html#adfccd2d2eedba4804b4c6f9d156847e5", null ],
    [ "CountDevicesUsed", "class_hardware___inventory_1_1_repository_1_1_repositories_1_1_devices_repository.html#a134c888dc2b603e0bdac284fb4b6eda9", null ],
    [ "Delete", "class_hardware___inventory_1_1_repository_1_1_repositories_1_1_devices_repository.html#a42642c455a497adc48cdd7ece1ea2371", null ],
    [ "GetAll", "class_hardware___inventory_1_1_repository_1_1_repositories_1_1_devices_repository.html#a7cfeabc75dad76f8dcc3f437c30cebc0", null ],
    [ "GetAllDeviceWithUser", "class_hardware___inventory_1_1_repository_1_1_repositories_1_1_devices_repository.html#ad490d2b35aa08639a867506b946d42d3", null ],
    [ "GetDeviceById", "class_hardware___inventory_1_1_repository_1_1_repositories_1_1_devices_repository.html#a27b69b5d4733044a3cc6f6cd1e480a9b", null ],
    [ "UpdateDevice", "class_hardware___inventory_1_1_repository_1_1_repositories_1_1_devices_repository.html#a2071001b8112051d32249d02df0a5ed2", null ]
];